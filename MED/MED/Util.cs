﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
using System.IO;

namespace MED
{
    #region descriptionClassesAndStructurs
    [DataContract]
    public class State
    {
        [DataMember]
        private List<Expert> experts;

        [DataMember]
        private List<Measure> measurments;

        [DataMember]
        private List<Criteria> criterias;

        [DataMember]
        private List<ScaleEntity> criterias_scale;

        [DataMember]
        private List<ScaleEntity> projects_scale;

        [DataMember]
        private int expertID;

        [DataMember]
        private int measureID;

        [DataMember]
        private int criteriaID;


        public State()
        {
            criterias = new List<Criteria>();
            experts = new List<Expert>();
            measurments = new List<Measure>();
            criterias_scale = new List<ScaleEntity>();
            projects_scale = new List<ScaleEntity>();
        }

        public void setIDs(int expertID, int measureID, int criteriaID)
        {
            this.expertID = expertID;
            this.criteriaID = criteriaID;
            this.measureID = measureID;
        }
        public void set_criteria_scale(List<ScaleEntity> newScale)
        {
            if (criterias_scale == null)
            {
                criterias_scale = new List<ScaleEntity>();
            }
            else
            {
                criterias_scale.Clear();
            }
            for (int i = 0; i < newScale.Count; i++)
            {
                criterias_scale.Add( newScale[i]);
            }
        }

        public void set_project_scale(List<ScaleEntity> newScale)
        {
            if (projects_scale == null)
            {
                projects_scale = new List<ScaleEntity>();
            }
            else
            {
                projects_scale.Clear();
            }
            for (int i = 0; i < newScale.Count; i++)
            {
                projects_scale.Add(newScale[i]);
            }
        }

        public void merge(State newState)
        {
            mergeCriteria(newState.criterias);
            mergeMeasure(newState.measurments);
            mergeExperts(newState.experts);
        }
        private void mergeCriteria(List<Criteria> newCriteria)
        {
            var updatedCriterias = new List<Criteria>();
            foreach (Criteria curr in newCriteria)
            {
                var resultFind = criterias.Find(x => x.id == curr.id);
                if (resultFind.id != 0)//found
                {
                    resultFind.name = curr.name;
                    updatedCriterias.Add(resultFind);
                }
                else
                    updatedCriterias.Add(curr);
            }
            criterias = updatedCriterias;
        }
        private void mergeExperts(List<Expert> newExperts)
        {
            var updatedExperts = new List<Expert>();
            foreach (var curr in newExperts)
            {
                var resultFind = experts.Find(x => x.Id == curr.Id);
                if (resultFind != null)
                {
                    //resultFind.CriteriaGrades = curr.CriteriaGrades;
                    //resultFind.ProjectGrades = curr.ProjectGrades;
                    resultFind.fio = curr.fio;
                    updatedExperts.Add(resultFind);
                }
                else
                {
                    updatedExperts.Add(curr);
                }
            }
            experts = updatedExperts;
        }
        private void mergeMeasure(List<Measure> newMeasure)
        {
            var updatedMeasurements = new List<Measure>();
            foreach (var curr in newMeasure)
            {
                var result = measurments.Find(x => x.id == curr.id);
                if (result.id != 0)//found  
                {
                    result.name = curr.name;
                    updatedMeasurements.Add(result);
                }
                else
                {
                    updatedMeasurements.Add(curr);
                }
            }
            measurments = updatedMeasurements;
        }


        public void addExpert(Expert expert)
        {
            experts.Add(expert);
        }
        public void addMeasure(Measure measure)
        {
            measurments.Add(measure);
        }
        public void addCriteria(Criteria criteria)
        {
            criterias.Add(criteria);
        }

        public List<Expert> getExperts()
        {
            return experts;
        }
        public List<Expert> getActiveExperts()
        {
            var activeExperts = new List<Expert>();
            for (int i = 0; i < experts.Count; i++ )
            {
                if (experts[i].Active == true) activeExperts.Add(experts[i]);
            }
            return activeExperts;
        }
        public List<Criteria> getCriterias()
        {
            return criterias;
        }
        public List<Measure> getMeasures()
        {
            return measurments;
        }
        public List<ScaleEntity> get_criteria_scale()
        {
            return criterias_scale;
        }

        public List<ScaleEntity> get_project_scale()
        {
            return projects_scale;
        }

        public bool equals(State newState)
        {
            if (this.criteriaID != newState.criteriaID
                && this.expertID != newState.expertID
                && this.measureID != newState.measureID)
                return false;

            if (!isCriteriaEquals(newState.criterias))
                return false;

            if (!isCriteriasScaleEquals(newState.criterias_scale))
                return false;

            if (!isProjectsScaleEquals(newState.projects_scale))
                return false;

            if (!isMeasurmentsEquals(newState.measurments))
                return false;

            if (!isExpertsEquals(newState.experts))
                return false;


            return true;
        }
        private bool isCriteriaEquals(List<Criteria> newCriterias)
        {

            if (this.criterias.Count != newCriterias.Count)
                return false;

            for (int i = 0; i < this.criterias.Count; i++)
            {
                if (   this.criterias[i].id != newCriterias[i].id 
                    || this.criterias[i].name != newCriterias[i].name)
                    return false;
            }
            return true;
        }
        private bool isMeasurmentsEquals(List<Measure> newMeasurments)
        {

            if (this.measurments.Count != newMeasurments.Count)
                return false;

            for (int i = 0; i < this.criterias.Count; i++)
            {
                if (   this.measurments[i].id != newMeasurments[i].id
                    || this.measurments[i].name != newMeasurments[i].name)
                    return false;
            }
            return true;
        }
        private bool isExpertsEquals(List<Expert> newExpert)
        {

            if (this.experts.Count != newExpert.Count)
                return false;

            for (int i = 0; i < this.experts.Count; i++)
            {
                if ( this.experts[i].Active != newExpert[i].Active 
                    || this.experts[i].Id != newExpert[i].Id )
                    return false;

                if (this.experts[i].fio.lastName != newExpert[i].fio.lastName
                    || this.experts[i].fio.name != newExpert[i].fio.name
                    || this.experts[i].fio.surName != newExpert[i].fio.surName)
                    return false;

                if ( ! MatrixСalculator.isMatrixEquals( this.experts[i].CriteriaGrades , newExpert[i].CriteriaGrades))
                    return false;

                if ( !MatrixСalculator.isMatrixEquals(this.experts[i].ProjectGrades, newExpert[i].ProjectGrades))
                    return false;

                //if (this.experts[i].Id != newExpert[i].Active)
                   // return false;
            }
            return true;
        }
        private bool isCriteriasScaleEquals(List<ScaleEntity> newCriterias_scale)
        {
            if (this.criterias_scale.Count != newCriterias_scale.Count)
                return false;

            for (int i = 0; i < this.criterias_scale.Count; i++)
            {
                if (this.criterias_scale[i].value != newCriterias_scale[i].value
                    || this.criterias_scale[i].interpretation != newCriterias_scale[i].interpretation)
                    return false;
            }
            return true;
        }
        private bool isProjectsScaleEquals(List<ScaleEntity> newProjects_scale)
        {
            if (this.projects_scale.Count != newProjects_scale.Count)
                return false;

            for (int i = 0; i < this.projects_scale.Count; i++)
            {
                if (this.projects_scale[i].value != newProjects_scale[i].value
                    || this.projects_scale[i].interpretation != newProjects_scale[i].interpretation)
                    return false;
            }
            return true;
        }

    }

    [DataContract]
    public struct FIO
    {
        [DataMember]
        public string name;

        [DataMember]
        public string lastName;

        [DataMember]
        public string surName;
    }

    [DataContract]
    public class Expert
    {
        public double[,] CriteriaGrades { get; set; }

        public double[,] ProjectGrades { get; set; }

        [DataMember]
        public FIO fio { get; set; }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public bool Active { get; set; }

        public Expert(FIO fio, int id)
        {
            this.fio = fio;
            Id = id;
            Active = true;
        }

        #region Преобразования матриц для сереализации
        [OnSerializing]
        public void BeforeSerializing(StreamingContext ctx)
        {
            if (this.CriteriaGrades != null)
                this.surrogateCriteriaGrades =
                    MatrixRoutines.convertPlainToNested(this.CriteriaGrades);

            if (this.ProjectGrades != null)
                this.surrogateProjectGrades =
                    MatrixRoutines.convertPlainToNested(this.ProjectGrades);
        }

        [OnDeserialized]
        public void AfterDeserializing(StreamingContext ctx)
        {
            //if (this.CriteriaGrades != null)
                this.CriteriaGrades =
                    MatrixRoutines.convertNestedToPlain(surrogateCriteriaGrades);
            //if (this.ProjectGrades != null)
                this.ProjectGrades =
                    MatrixRoutines.convertNestedToPlain(surrogateProjectGrades);
        }

        [DataMember]
        private double[][] surrogateCriteriaGrades;

        [DataMember]
        private double[][] surrogateProjectGrades;

        #endregion
        
    }

    [DataContract]
    public struct Measure
    {
        [DataMember]
        public int id;

        [DataMember]
        public String name;
    }

    [DataContract]
    public struct Criteria
    {
        [DataMember]
        public int id;

        [DataMember]
        public String name { get; set; }
    }

    [DataContract]
    
    #endregion
   
    public static class DataGridViewUtils
    {
        public static DataGridView generateTable(List<Tuple<String, String>> namesAndHeaders)
        {
            var grid = new DataGridView();
            grid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            grid.Dock = DockStyle.Fill;
            foreach (var n in namesAndHeaders)
            {
                if (n.Item2 == "ID")
                {
                    grid.Columns.Add(n.Item1, n.Item2);
                    //grid.Columns[n.Item1].Visible = false;
                }
                else
                    grid.Columns.Add(n.Item1, n.Item2);
            }

            return grid;
        }

        public static double[,] getMatrixGrades(DataGridView table)
        {
            int rowCount = table.RowCount;
            int colCount = table.ColumnCount - 1;
            double[,] matrixGrades = new double[rowCount, colCount];
            for (int row = 0; row < rowCount; row++)
                for (int col = 0; col < colCount; col++)
                {
                    string valueCells = table.Rows[row].Cells[col + 1].FormattedValue.ToString();
                    if (valueCells != "")
                    {
                        //valueCells = GUIUtils.replacementCommasOnPointInString( valueCells);
                        var elementMatrix = double.Parse(valueCells);
                        matrixGrades[row, col] = elementMatrix;
                    }
                    else matrixGrades[row, col] = -987654321;
                }
            return matrixGrades;
        }
        public static List<ScaleEntity> buildScale(DataGridView table)
        {
            var newScale = new List<ScaleEntity>();
            for (int i = 0; i < table.RowCount; i++)
            {
                if (table.Rows[i].Cells[0].Value != null)
                {
                    ScaleEntity s = new ScaleEntity();
                    s.value = double.Parse( table.Rows[i].Cells[0].Value.ToString());//double.Parse(GUIUtils.replacementCommasOnPointInString(table.Rows[i].Cells[0].Value.ToString()))
                    s.interpretation = table.Rows[i].Cells[1].Value.ToString();
                    newScale.Add(s);
                }
            }
            return newScale;
        }

        public static bool isTableColsValid(DataGridView table, String[] colsNames)
        {
            for (var i = 0; i < table.RowCount - 1; i++)
            {
                var row = table.Rows[i];
                foreach (var colName in colsNames)
                {
                    var tableCell = row.Cells[table.Columns[colName].Index];
                    var cellOk = DataGridViewUtils.isNameNonEmpty(tableCell);
                    if (!cellOk) return false;
                }
            }
            return true;
        }
        public static bool isNameNonEmpty(DataGridViewCell cell)
        {
            return (cell.Value != null && cell.Value.ToString().Length != 0);
        }

        public static void resetGradesCriteria(DataGridView dgv)
        {
            for (int colum = 1; colum < dgv.ColumnCount; colum++)
                for (int row = 0; row < dgv.RowCount; row++)
                    if (colum != row + 1)
                        dgv.Rows[row].Cells[colum].Value = "";
        }
        public static void resetGradesMeasure(DataGridView dgv)
        {
            for (int colum = 1; colum < dgv.ColumnCount; colum++)
                for (int row = 0; row < dgv.RowCount; row++)  
                        dgv.Rows[row].Cells[colum].Value = "";
        }

        public static void dissallowEditingCells(DataGridView datagrid)
        {
            for (var row = 0; row < datagrid.RowCount; row++)
                for (var col = 0; col < datagrid.ColumnCount; col++)
                    if (row + 1 >= col)
                        datagrid.Rows[row].Cells[col].ReadOnly = true;
        }

        public static void addRowsName(DataGridView datagrid, List<Criteria> criteria)
        {
            for (var i = 0; i < criteria.Count; i++)
                datagrid.Rows.Add(criteria[i].name);
        }

        public static void SelIndChanged(DataGridView dgw, double[,] grades, Action<DataGridView> resetProc)
        {
            if (grades == null)
                resetProc(dgw);
            else
            {
                try
                {
                    for (var row = 0; row < dgw.RowCount && row < grades.GetLength(0); row++)
                        for (var col = 1; col <= grades.GetLength(1); col++)
                        {
                            if (grades[row, col - 1] != -987654321)
                            {
                                dgw.Rows[row].Cells[col].Value = grades[row, col - 1].ToString();
                            }
                            else
                            {
                                dgw.Rows[row].Cells[col].Value = "";
                            }
                        }
                }
                catch
                {
                    for (var row = 0; row < dgw.RowCount; row++)
                        for (var col = 1; col < dgw.ColumnCount; col++)
                            if (row != col) dgw.Rows[row].Cells[col].Value = "";
                }
            }
        }

        public static void addStandartScaleInRow(DataGridView table)
        {
            foreach (var a in ScaleUtils.standartScale())
                table.Rows.Add(a.value.ToString(), a.interpretation);
        }
        public static void addTenBallScaleInRow(DataGridView table)
        {
            foreach (var a in ScaleUtils.tenBallScale())
                table.Rows.Add(a.value.ToString(), a.interpretation);
        }
        public static void addStateScaleInRow(DataGridView table, List<ScaleEntity> scale)
        {
            if (scale != null)
                foreach (var valueRow in scale)
                    table.Rows.Add(valueRow.value.ToString(), valueRow.interpretation);
        }
    }

    public static class ScaleUtils
    {
        #region Стандартная и десятибальная шкалы
        public static List<ScaleEntity> standartScale()
        {
            var scale = new List<ScaleEntity>();
            scale.Add(new ScaleEntity(0, "критерии равнозначны"));
            scale.Add(new ScaleEntity(0.5, "критерий в строке слабо доминирует над критерием в столбце"));
            scale.Add(new ScaleEntity(1, "критерий в строке сильно доминирует над критерием в столбце"));
            return scale;
        }
        public static List<ScaleEntity> tenBallScale()
        {
            var scale = new List<ScaleEntity>();
            scale.Add(new ScaleEntity(0, "Объекты не сравнимы"));
            scale.Add(new ScaleEntity(1, "Объекты одинаково важны"));
            scale.Add(new ScaleEntity(3, "Умеренное превосходство одного над другим "));
            scale.Add(new ScaleEntity(5, "Существенное превосходство одного над другим "));
            scale.Add(new ScaleEntity(7, "Значительное превосходство одного над другим "));
            scale.Add(new ScaleEntity(9, "Абсолютное превосходство одного над другим "));
            return scale;
        }
        #endregion
        public static double[,] getMatrixInTenBallScale(double[,] matrix, List<ScaleEntity> scale)
        {
            var colCount = matrix.GetLength(1);
            var rowCount = matrix.GetLength(0);
            double[,] result = new double[rowCount, colCount];
            double distanceMinElementMatrixToZero = 0 - minValueScale(scale);
            double distanceMaxElementMatrix = maxValueScale(scale) + distanceMinElementMatrixToZero;
            for (int i = 0; i < rowCount; i++)
                for (int j = 0; j < colCount; j++)
                    if (i < j)
                    {
                        result[i, j] = (matrix[i, j] + distanceMinElementMatrixToZero);
                        result[i, j] = result[i, j] * (8.0 / distanceMaxElementMatrix);
                        result[i, j] = result[i, j] + 1;
                    }
            return result;
        }

        public static string nameScale(List<ScaleEntity> scale)
        {
            if (ScaleUtils.isStandartScale(scale))
                return "standart";
            if (ScaleUtils.isTenBallScale(scale))
                return "tenBall";
            return "users";
        }
        public static string tipTextScale(List<ScaleEntity> scale)
        {
            string tips = "";
            foreach (var elemScale in scale)
                tips += elemScale.value.ToString() + " ( " + elemScale.interpretation + " ) \n";
            return tips;
        }

        public static bool isScaleNullOrCountNull(List<ScaleEntity> scale)
        {
            if (scale == null) return true;
            if (scale.Count == 0) return true;
            return false;
        }

        public static bool isValueScaleExact(List<ScaleEntity> scale, double value)
        {
            if (isScaleNullOrCountNull(scale)) return false;
            foreach (var elemScale in scale)
                if (elemScale.value == value) return true;
            return false;
        }
        public static bool isValueWithInLimitsScale(List<ScaleEntity> scale, double value)
        {
            if (isScaleNullOrCountNull(scale)) return false;
            if (value <= maxValueScale(scale) && value >= minValueScale(scale)) return true;
            return false;
        }
      
        public static bool isStandartScale(List<ScaleEntity> scale)
        {
            int cur = 0;
            foreach (var a in scale) if (isElementInScale(a, standartScale())) cur++;
            if (cur == scale.Count) return true;
            return false;
        }
        public static bool isTenBallScale(List<ScaleEntity> scale)
        {
            int cur = 0;
            foreach (var a in scale) if (isElementInScale(a, tenBallScale())) cur++;
            if (cur == scale.Count) return true;
            return false;
        }
        public static bool isElementInScale(ScaleEntity element, List<ScaleEntity> ScaleList)
        {
            foreach (var elemInList in ScaleList)
                if (elemInList.interpretation == element.interpretation && elemInList.value == element.value)
                    return true;
            return false;
        }

        public static double maxValueScale(List<ScaleEntity> scale)
        {
            double max = scale[0].value;
            foreach (var elemScale in scale)
                if (max < elemScale.value)
                    max = elemScale.value;
            return max;
        }
        public static double minValueScale(List<ScaleEntity> scale)
        {
            double min = scale[0].value;
            foreach (var elemScale in scale)
                if (min > elemScale.value)
                    min = elemScale.value;
            return min;
        }

        /// <summary>
        /// Проверяет, принадлежит ли значение заданной шкале.
        /// </summary>
        public static bool is_value_in_criteria_scale(List<ScaleEntity> scale, double value)
        {
            var query = from entity in scale
                        select (entity.value == value || entity.value == -value);

            return query.Any(x => x == true);
        }

        public static bool is_value_in_project_scale(List<ScaleEntity> scale, double value)
        {
            var query = from entity in scale
                        select entity.value == value;

            return query.Any(x => x == true);
        }

        /// <summary>
        /// Возвращает все элементы шкалы в виде массва.
        /// </summary>
        public static double[] scale_values(List<ScaleEntity> scale)
        {
            var query = from entity in scale
                        orderby entity.value
                        select entity.value;

            return query.ToArray();
        }
    }

    public static class GUIUtils
    {
        public static ComboBox createExpertsCB(State state)
        {
            var combobox = new ComboBox();
            foreach (var e in state.getExperts())
                combobox.Items.Add(e.fio.lastName + " " + e.fio.name + " " + e.fio.surName);
            combobox.SelectedItem = combobox.Items[0];
            combobox.DropDownStyle = ComboBoxStyle.DropDownList;
            return combobox;
        }

        public static List<Tuple<String, String>> createCriteriaColumnsNames(List<Criteria> criterias)
        {
            var columns = new List<Tuple<String, String>>();

            columns.Add(new Tuple<string, string>("no_sence_name", ""));

            for (var col = 0; col < criterias.Count; col++)
            {
                var name = criterias[col].name;
                columns.Add(new Tuple<string, string>(name, name));
            }

            return columns;
        }
        public static List<Tuple<String, String>> createMeasuresColumnsNames(List<Measure> measures)
        {
            var columns = new List<Tuple<String, String>>();

            columns.Add(new Tuple<string, string>("no_sence_name", ""));

            for (var col = 0; col < measures.Count; col++)
            {
                var name = measures[col].name;
                columns.Add(new Tuple<string, string>(name, name));
            }

            return columns;
        }

        /*public static string replacementCommasOnPointInString(string OutString)
        {
            string newString = "";
            for (int i = 0; i < OutString.Count(); i++ )
            {
                if (OutString[i] == '.')
                    newString += ',';
                else
                    newString += OutString[i];
            }
            return newString;
        }*/
    }

    public class Saver
    {
        private static readonly Encoding LocalEncoding = Encoding.UTF8;

        public static void saveStateToFile(string pathToFile, State state)
        {
            var stream = new MemoryStream();
            var ser = new DataContractJsonSerializer(typeof(State));
            ser.WriteObject(stream, state);
            var s = LocalEncoding.GetString(stream.ToArray());

            using (FileStream fs = File.Create(pathToFile))
            {
                AddText(fs, s);
            }
        }

        public static State readStateFromFile(string pathToFile)
        {
            var ser = new DataContractJsonSerializer(typeof(State));
            FileStream fsStream = File.OpenRead(pathToFile);

            var state = (State)ser.ReadObject(fsStream);
            return state;
        }

        private static void AddText(FileStream fs, string value)
        {
            byte[] info = new UTF8Encoding(true).GetBytes(value);
            fs.Write(info, 0, info.Length);
        }
    }


    public static class Algorithm
    {
        public static double[]  projectsImportanceAvg(double[,] prjImp)
        {
            var rowCount = prjImp.GetLength(0);
            var colCount = prjImp.GetLength(1);
            double[] result = new double[colCount];

            for (var col = 0; col < colCount; col++)
            {
                result[col] = getColumnOfMatrix(prjImp, col).Average();
            }

            return result;
        }
        public static double[,] projectsImportance(State a)
        {
            var projects = a.getMeasures();
            var experts = a.getExperts();
            double[][] projImportanceForEachExpert = new double[getActiveExpertsCount(experts)][];//HERE experts.Count -> getActiveExperts(experts)
            for (var i = 0; i < getActiveExpertsCount(experts); i++)//HERE experts.Count -> getActiveExperts(experts)
                 projImportanceForEachExpert[i] = new double[projects.Count];

            var expertCounter = 0;
            foreach (var expert in experts)
            {
                if (expert.Active)//HERE add if (expert.Active)
                {
                    var projGrades = expert.ProjectGrades;
                    var critGrades = expert.CriteriaGrades;

                    var maxValueScale = ScaleUtils.maxValueScale(a.get_criteria_scale());
                    var minValueScale = ScaleUtils.minValueScale(a.get_criteria_scale());

                    var normalizeMatrix_ = normalizeMatrix(critGrades, maxValueScale, minValueScale);

                    var critImportance = criteriaImportance(normalizeMatrix_);
                    var projImportance = projectImportance(projGrades, critImportance);
                    projImportanceForEachExpert[expertCounter] = projImportance;
                    expertCounter++;
                }
            }

            return conv(projImportanceForEachExpert);
        }
        public static int getActiveExpertsCount(List<Expert> experts)
        {
            int countActiveExperts = 0;
            foreach (var expert in experts)
            {
                if (expert.Active) countActiveExperts++;
            }
            return countActiveExperts;
        }
        public static Tuple<double,string> concordationCoefName(double[,] projImportance)
        {
            var result = new Tuple<double,string>(0,"1");
            var average = projectsImportanceAvg(projImportance).Average();
            var colCount = projImportance.GetLength(1);
            var rowCount = projImportance.GetLength(0);

            double S = 0;

            for (var col = 0; col < colCount; col++)
            {
                double sum = 0;
                for (var row = 0; row < rowCount; row++)
                {
                    sum += projImportance[row, col] - average;
                }
                sum *= sum;
                S += sum;
            }

            double concard = (12 / (Math.Pow(rowCount, 2) * (Math.Pow(colCount, 3) - colCount))) * S;

            if (concard < 0.3) return new Tuple<double, string>(concard, "Слабая");
            if (concard < 0.5) return new Tuple<double, string>(concard, "Умеренная");
            if (concard < 0.7) return new Tuple<double, string>(concard, "Заметная");
            if (concard < 0.9) return new Tuple<double, string>(concard, "Высокая");
            else return new Tuple<double, string>(concard, "Очень высокая");
        }
        public static double[] expertsCompetence(double[,] projImportance)
        {
            var resultVector = Algorithm.projectsImportanceAvg(projImportance);
            var rowCount = projImportance.GetLength(0);
            var colCount = projImportance.GetLength(1);
            double[] result = new double[rowCount];

            for (var row = 0; row < rowCount; row++) 
            {
                double sum = 0;
                for (var col = 0; col < colCount; col++)
                {
                    sum += Math.Pow(resultVector[col] - projImportance[row,col],2);                   
                }
                result[row] = sum;
            }

            var sumResult = result.Sum();
            if (sumResult != 0)
            {
                for (var i = 0; i < rowCount; i++)
                {
                    result[i] /= sumResult;
                }
            }

            //result = MatrixСalculator.reversRow(result);
            return result;
        }

        private static double[] projectImportance(double[,] projectsGrades,
                                                  double[]  critImportance)
        {
            var projectsCount = projectsGrades.GetLength(1);
            double[] weights = new double[projectsCount];
 
            for (var project = 0; project < projectsCount; project++)
            {
                var col = getColumnOfMatrix(projectsGrades, project);
                weights[project] = calcWeigth(col, critImportance);
            }

            return weights;
        }


        private static double calcWeigth(double[] criterias, double[] weights)
        {
            var result = 0.0;
            var length = weights.GetLength(0);
            for (var i = 0; i < length; i++)
            {
                result += weights[i] * Math.Pow((1 - criterias[i]), 2.0);
            }

            return Math.Sqrt(result);
        }

        private static double[] criteriaImportance(double[,] critNorm)
        {
            var colCount = critNorm.GetLength(0);
            double[] importance = new double[colCount];

            for (var i = 0; i < colCount; i++)
            {
                var s = critNorm.Cast<double>().Skip(i * colCount).Take(colCount);
                importance[i] = s.Average();
            }

            return importance;                        
        }

        private static double[,] normalizeMatrix(double[,] matrix, 
                                                 double intervalMax, 
                                                 double intervalMin)
        {
            var colCount = matrix.GetLength(0);
            var rowCount = matrix.GetLength(1);
            double[,] result = new double[colCount, rowCount];

            if (intervalMin >= 0)
            {
                double dist = 0 - intervalMin;

                for (var row = 0; row < rowCount; row++)
                    for (var col = 0; col < colCount; col++)
                    {
                        if (col == row)
                            result[row, col] = 0;
                        else
                        {
                            if (isElementMatrixAboveDiagonal(row, col))
                                result[row, col] = (double)(matrix[row, col] + dist + Math.Abs(intervalMin - intervalMax)) / (2 * Math.Abs(intervalMin - intervalMax));
                            else
                                result[row, col] = (double)(matrix[row, col] - dist + Math.Abs(intervalMin - intervalMax)) / (2 * Math.Abs(intervalMin - intervalMax));
                        }
                    }
            }
            else
            {
                double max = 0;
                if (Math.Abs(intervalMin) <= Math.Abs(intervalMax))
                {
                    max = Math.Abs(intervalMax);
                }
                else
                {
                    max = Math.Abs(intervalMin);
                }

                for (var row = 0; row < rowCount; row++)
                    for (var col = 0; col < colCount; col++)
                    {
                        if (col == row)
                            result[row, col] = 0;
                        else
                        {
                            result[row, col] = (double)(matrix[row, col] + max) / (2 * max);
                        }
                    }
            }
            return result;
        }
        private static bool isElementMatrixAboveDiagonal(int indexRow, int indexCol)
        {
            if (indexRow < indexCol) return true;
            else return false;
        }
        private static double[] getColumnOfMatrix(double[,] a, int columnNum)
        {
            var result = new double[a.GetLength(0)];
            for (var row = 0; row < a.GetLength(0); row++)
                result[row] = a[row, columnNum];
            return result;
        }

        public static void tests()
        {
            assureThat(testNormMatrix, "Matrix Normalization test");

            assureThat(testCriteriaImportance, "Criteria Importance test");
        }

        private static bool testNormMatrix()
        {
            double[,] testData = { { 0, 1, 1 }, { -1, 0, 1 }, { -1, -1, 0 } };
            double[,] etalon = { { 0, 1, 1 }, { 0, 0, 1 }, { 0, 0, 0 } };
            var result = normalizeMatrix(testData, 1, -1);

            return compareMatrix(result, etalon, 0.01);
        }

        private static bool testCriteriaImportance()
        {
            double[,] testData = { { 0, 1, 1 }, { 0, 0, 1 }, { 0, 0, 0 } };
            double[] ethalon = { 0.666666, 0.33333, 0 };
            var result = criteriaImportance(testData);

            return compareMatrix(result, ethalon, 0.01);
        }

        private static bool compareMatrix(double[,] a, double[,] b, double eps)
        {
            var colCount = a.GetLength(0);
            var rowCount = a.GetLength(1);

            for (int i = 0; i < rowCount; i++)
                for (int j = 0; j < colCount; j++)
                    if (Math.Abs(a[i, j] - b[i, j]) >= eps)
                        return false;
            return true;
        }

        private static bool compareMatrix(double[] a, double[] b, double eps)
        {
            var colCount = a.GetLength(0);

            for (int i = 0; i < colCount; i++)
                    if (Math.Abs(a[i] - b[i]) >= eps)
                        return false;
            return true;
        }

        private static void assureThat(Func<bool> test, string testName)
        {
            if (!test()) MessageBox.Show("Fuck you in " + testName);
            else MessageBox.Show("Good boy in " + testName);
        }

        private static double[,] conv(double[][] a)
        {
            var rowCount = a.GetLength(0);
            var colCount = a[0].GetLength(0);
            double[,] result = new double[rowCount, colCount];

            for (var i = 0; i < rowCount; i++)
                for (var j = 0; j < colCount; j++)
                    result[i, j] = a[i][j];
            return result;
        }
    }

    public static class MatrixСalculator
    {
        
        public static double[,] perestanovka(double[,] projImportance)
        {
            var colCount = projImportance.GetLength(1);
            var rowCount = projImportance.GetLength(0);
            double[,] result = new double[rowCount, colCount];
            for (int i = 0; i < rowCount; i++)
            {
                var row = getRowOfMatrix(projImportance, i);
                var resultRow = getRowOfMatrix(result, i);
                var reversrowe =  reversRow(row);
                for (int j = 0; j < colCount; j++)
                    result[i, j] = reversrowe[j];
            }

            return result;
        }
        public static double[] reversRow(double[] a)
        {
            var result = new double[a.GetLength(0)];
            int[] namberElementNoSort = new int[a.GetLength(0)];

            List<int> poll = new List<int>();
            double mod = a.GetLength(0) % 2;
            int countIteration = a.GetLength(0);
            if (mod != 0)
            {
                string ab = (((double)a.GetLength(0) / 2) - 0.5).ToString();
                countIteration = int.Parse(ab);
            }
            else
            {
                countIteration = countIteration / 2;
            }
            int minInRow = 0;
            int maxInRow = 0;
            for (int i = 0; i < countIteration; i++)
            {
                minInRow = findMinIndex(a, poll);
                maxInRow = fimdMaxIndex(a, poll);
                double curr = a[minInRow];
                result[maxInRow] = a[minInRow];
                result[minInRow] = a[maxInRow];
                //a[minInRow] = a[maxInRow];
                //a[maxInRow] = curr;
                poll.Add(minInRow);
                poll.Add(maxInRow);
            }
            return result;
        }
        private static double[] getColumnOfMatrix(double[,] a, int columnNum)
        {
            var result = new double[a.GetLength(0)];
            for (var row = 0; row < a.GetLength(0); row++)
                result[row] = a[row, columnNum];
            return result;
        }
        private static double[] getRowOfMatrix(double[,] a, int rowNum)
        {
            var result = new double[a.GetLength(1)];
            for (var col = 0; col < a.GetLength(1); col++)
                result[col] = a[rowNum, col];
            return result;
        }
        private static int findMinIndex(double[] a, List<int> poll)
        {
            double min = a.Max();
            int indexMin = 0;
            for (int i = 0; i < a.GetLength(0); i++)
            {
                if (!isInPool(poll, i))
                {
                    if (a[i] < min)
                    {
                        min = a[i];
                        indexMin = i;
                    }
                }
            }
            return indexMin;
        }
        private static int fimdMaxIndex(double[] a, List<int> poll)
        {
            double max = a.Min();
            int indexmax = 0;
            for (int i = 0; i < a.GetLength(0); i++)
            {
                if (!isInPool(poll,i))
                {
                    if (a[i] > max)
                    {
                        max = a[i];
                        indexmax = i;
                    }
                }
            }
            return indexmax;
        }
        private static bool isInPool(List<int> poll, int i)
        {
            foreach (var a in poll)
                if (a == i) return true;
            return false;
        }
        public static double[,] rangMatrix(double[,] matrixGreads)
        {
            int rowCount = matrixGreads.GetLength(0);
            int colCount = matrixGreads.GetLength(1);
            double[,] result = new double[rowCount, colCount];
            for (int i = 0; i < rowCount; i++)
            {
                var rowOfMatrix = getRowOfMatrix(matrixGreads,i);
                var rangsrowOfMatrix = rangsRowMatrix(rowOfMatrix);
                for (int j = 0; j < colCount; j++)
                {
                    result[i, j] = rangsrowOfMatrix[j];
                }
            }
            return result;
        }
        public static double[] rangsRowMatrix(double[] matrixRow)
        {
            int count = matrixRow.GetLength(0);
            double[] result = new double[count];
            double[] sortMatrixRow = new double[count];
            for (int i = 0; i < count; i++)
                sortMatrixRow[i] = matrixRow[i];
            Array.Sort(sortMatrixRow);
            for (int i = 0; i < count; i++)
            {
                double elem = matrixRow[i];
                var counElemInRow = countElements(sortMatrixRow, elem);
                var rang = 0;
                for (int j = 0; j < counElemInRow; j++ )
                    rang += indexElementsInRow(sortMatrixRow, elem) + j + 1;
                result[i] = (double)(rang) / counElemInRow;
            }
            return result;
        }
        public static int indexElementsInRow(double[] matrixRow, double elem)
        {
            int count = 0;
            foreach (var a in matrixRow)
            {
                if (a == elem) return count;
                count++;
            }
            return count;
        }
        public static int countElements(double[] matrixRow, double elem)
        {
            int count = 0;
            foreach (var a in matrixRow)
                if (a == elem) count++;
            return count;
        }

        public static bool isMatrixEquals(double[,] a, double[,] b)
        {
            if (a == null || b == null) 
                return false;
            if (a.GetLength(0) != b.GetLength(0)
                || a.GetLength(1) != b.GetLength(1))
                return false;

            for (int i = 0; i < a.GetLength(0); i++)
                for (int j = 0; j < a.GetLength(1); j++)
                {
                    if (a[i, j] != b[i, j])
                        return false;
                }

            return true;
        }
    }
}
/*
  private static double[,] normalizeMatrix(double[,] matrix, 
                                                 double intervalMax, 
                                                 double intervalMin)
        {
            var colCount = matrix.GetLength(0);
            var rowCount = matrix.GetLength(1);
            double[,] result = new double[colCount, rowCount];

            for (var row = 0; row < rowCount; row++)
                for (var col = 0; col < colCount; col++)
                {
                    if (col == row)
                    {
                        result[row, col] = 0;
                    }
                    else
                    {
                        // Math.Abs(intervalMin))) 
                        double dist = 0 - intervalMin;
                        result[row, col] = (double)(matrix[row, col] + dist + intervalMax);// /
                            //Math.Abs(intervalMin - intervalMax);
                    }
                }
            return result;
        }
*/
