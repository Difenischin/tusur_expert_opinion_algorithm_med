﻿namespace MED
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.оПрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonCriteria = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonMeasure = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.info = new System.Windows.Forms.ToolStripStatusLabel();
            this.СhiefContainer = new System.Windows.Forms.SplitContainer();
            this.GradesControl = new System.Windows.Forms.TabControl();
            this.GradesCriteriaPage = new System.Windows.Forms.TabPage();
            this.gradesCriteriaPanel = new System.Windows.Forms.Panel();
            this.GradesMeasurePage = new System.Windows.Forms.TabPage();
            this.gradesMeasurePanel = new System.Windows.Forms.Panel();
            this.resultContainer = new System.Windows.Forms.SplitContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.getResult = new System.Windows.Forms.Button();
            this.resultExpertBox = new System.Windows.Forms.Panel();
            this.scale_control = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.scale_criteria_panel = new System.Windows.Forms.Panel();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.scale_projects_panel = new System.Windows.Forms.Panel();
            this.toolStripButtonExpert = new System.Windows.Forms.ToolStripButton();
            this.Scale = new System.Windows.Forms.ToolStripButton();
            this.Grades = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonResults = new System.Windows.Forms.ToolStripButton();
            this.новыйПроектToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.открытьПроектToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.СhiefContainer)).BeginInit();
            this.СhiefContainer.Panel2.SuspendLayout();
            this.СhiefContainer.SuspendLayout();
            this.GradesControl.SuspendLayout();
            this.GradesCriteriaPage.SuspendLayout();
            this.GradesMeasurePage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resultContainer)).BeginInit();
            this.resultContainer.Panel1.SuspendLayout();
            this.resultContainer.SuspendLayout();
            this.scale_control.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.оПрограммеToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip.Size = new System.Drawing.Size(919, 24);
            this.menuStrip.TabIndex = 1;
            this.menuStrip.Text = "menuStrip";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.новыйПроектToolStripMenuItem,
            this.toolStripSeparator3,
            this.открытьПроектToolStripMenuItem,
            this.saveFileToolStripMenuItem,
            this.сохранитьToolStripMenuItem,
            this.toolStripSeparator4,
            this.выходToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(162, 6);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(162, 6);
            // 
            // оПрограммеToolStripMenuItem
            // 
            this.оПрограммеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.справкаToolStripMenuItem});
            this.оПрограммеToolStripMenuItem.Name = "оПрограммеToolStripMenuItem";
            this.оПрограммеToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
            this.оПрограммеToolStripMenuItem.Text = "О программе";
            // 
            // toolStrip
            // 
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonExpert,
            this.toolStripButtonCriteria,
            this.toolStripButtonMeasure,
            this.toolStripSeparator1,
            this.Scale,
            this.Grades,
            this.toolStripSeparator2,
            this.toolStripButtonResults});
            this.toolStrip.Location = new System.Drawing.Point(0, 24);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(919, 25);
            this.toolStrip.TabIndex = 2;
            this.toolStrip.Text = "toolStrip1";
            // 
            // toolStripButtonCriteria
            // 
            this.toolStripButtonCriteria.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCriteria.Name = "toolStripButtonCriteria";
            this.toolStripButtonCriteria.Size = new System.Drawing.Size(64, 22);
            this.toolStripButtonCriteria.Text = "Критерии";
            this.toolStripButtonCriteria.Click += new System.EventHandler(this.toolStripButtonCriteria_Click);
            // 
            // toolStripButtonMeasure
            // 
            this.toolStripButtonMeasure.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonMeasure.Name = "toolStripButtonMeasure";
            this.toolStripButtonMeasure.Size = new System.Drawing.Size(60, 22);
            this.toolStripButtonMeasure.Text = "Проекты";
            this.toolStripButtonMeasure.Click += new System.EventHandler(this.toolStripButtonMeasure_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // mainPanel
            // 
            this.mainPanel.Location = new System.Drawing.Point(0, 51);
            this.mainPanel.Margin = new System.Windows.Forms.Padding(2);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(94, 476);
            this.mainPanel.TabIndex = 3;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "json|*.json";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "json|*.json";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.info});
            this.statusStrip1.Location = new System.Drawing.Point(0, 510);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(919, 22);
            this.statusStrip1.TabIndex = 22;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // info
            // 
            this.info.Name = "info";
            this.info.Size = new System.Drawing.Size(0, 17);
            // 
            // СhiefContainer
            // 
            this.СhiefContainer.Location = new System.Drawing.Point(127, 57);
            this.СhiefContainer.Name = "СhiefContainer";
            // 
            // СhiefContainer.Panel1
            // 
            this.СhiefContainer.Panel1.AutoScroll = true;
            // 
            // СhiefContainer.Panel2
            // 
            this.СhiefContainer.Panel2.Controls.Add(this.GradesControl);
            this.СhiefContainer.Size = new System.Drawing.Size(173, 475);
            this.СhiefContainer.SplitterDistance = 30;
            this.СhiefContainer.TabIndex = 23;
            // 
            // GradesControl
            // 
            this.GradesControl.Controls.Add(this.GradesCriteriaPage);
            this.GradesControl.Controls.Add(this.GradesMeasurePage);
            this.GradesControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GradesControl.Location = new System.Drawing.Point(0, 0);
            this.GradesControl.Name = "GradesControl";
            this.GradesControl.Padding = new System.Drawing.Point(0, 0);
            this.GradesControl.SelectedIndex = 0;
            this.GradesControl.Size = new System.Drawing.Size(139, 475);
            this.GradesControl.TabIndex = 0;
            this.GradesControl.Selected += new System.Windows.Forms.TabControlEventHandler(this.GradesControl_Selected);
            // 
            // GradesCriteriaPage
            // 
            this.GradesCriteriaPage.Controls.Add(this.gradesCriteriaPanel);
            this.GradesCriteriaPage.Location = new System.Drawing.Point(4, 22);
            this.GradesCriteriaPage.Name = "GradesCriteriaPage";
            this.GradesCriteriaPage.Size = new System.Drawing.Size(131, 449);
            this.GradesCriteriaPage.TabIndex = 1;
            this.GradesCriteriaPage.Text = "Оценка критериев";
            this.GradesCriteriaPage.UseVisualStyleBackColor = true;
            // 
            // gradesCriteriaPanel
            // 
            this.gradesCriteriaPanel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.gradesCriteriaPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gradesCriteriaPanel.Location = new System.Drawing.Point(0, 0);
            this.gradesCriteriaPanel.Name = "gradesCriteriaPanel";
            this.gradesCriteriaPanel.Size = new System.Drawing.Size(131, 449);
            this.gradesCriteriaPanel.TabIndex = 0;
            // 
            // GradesMeasurePage
            // 
            this.GradesMeasurePage.Controls.Add(this.gradesMeasurePanel);
            this.GradesMeasurePage.Location = new System.Drawing.Point(4, 22);
            this.GradesMeasurePage.Name = "GradesMeasurePage";
            this.GradesMeasurePage.Size = new System.Drawing.Size(131, 449);
            this.GradesMeasurePage.TabIndex = 0;
            this.GradesMeasurePage.Text = "Оценка проектов";
            this.GradesMeasurePage.UseVisualStyleBackColor = true;
            // 
            // gradesMeasurePanel
            // 
            this.gradesMeasurePanel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.gradesMeasurePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gradesMeasurePanel.Location = new System.Drawing.Point(0, 0);
            this.gradesMeasurePanel.Name = "gradesMeasurePanel";
            this.gradesMeasurePanel.Size = new System.Drawing.Size(131, 449);
            this.gradesMeasurePanel.TabIndex = 0;
            // 
            // resultContainer
            // 
            this.resultContainer.Location = new System.Drawing.Point(318, 57);
            this.resultContainer.Name = "resultContainer";
            // 
            // resultContainer.Panel1
            // 
            this.resultContainer.Panel1.Controls.Add(this.label1);
            this.resultContainer.Panel1.Controls.Add(this.getResult);
            this.resultContainer.Panel1.Controls.Add(this.resultExpertBox);
            this.resultContainer.Size = new System.Drawing.Size(418, 450);
            this.resultContainer.SplitterDistance = 106;
            this.resultContainer.TabIndex = 24;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(159, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Учитывать мнение экспертов";
            // 
            // getResult
            // 
            this.getResult.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.getResult.Location = new System.Drawing.Point(0, 422);
            this.getResult.Name = "getResult";
            this.getResult.Size = new System.Drawing.Size(106, 28);
            this.getResult.TabIndex = 2;
            this.getResult.Text = "Вычислить";
            this.getResult.UseVisualStyleBackColor = true;
            this.getResult.Click += new System.EventHandler(this.getResult_Click);
            // 
            // resultExpertBox
            // 
            this.resultExpertBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.resultExpertBox.AutoScroll = true;
            this.resultExpertBox.Location = new System.Drawing.Point(3, 22);
            this.resultExpertBox.Name = "resultExpertBox";
            this.resultExpertBox.Size = new System.Drawing.Size(100, 393);
            this.resultExpertBox.TabIndex = 0;
            // 
            // scale_control
            // 
            this.scale_control.Controls.Add(this.tabPage1);
            this.scale_control.Controls.Add(this.tabPage2);
            this.scale_control.Location = new System.Drawing.Point(751, 57);
            this.scale_control.Name = "scale_control";
            this.scale_control.SelectedIndex = 0;
            this.scale_control.Size = new System.Drawing.Size(156, 422);
            this.scale_control.TabIndex = 25;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.scale_criteria_panel);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(148, 396);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Шкала оценки критериев";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // scale_criteria_panel
            // 
            this.scale_criteria_panel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.scale_criteria_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scale_criteria_panel.Location = new System.Drawing.Point(3, 3);
            this.scale_criteria_panel.Name = "scale_criteria_panel";
            this.scale_criteria_panel.Size = new System.Drawing.Size(142, 390);
            this.scale_criteria_panel.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.scale_projects_panel);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(148, 396);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Шкала оценки пректов";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // scale_projects_panel
            // 
            this.scale_projects_panel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.scale_projects_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scale_projects_panel.Location = new System.Drawing.Point(3, 3);
            this.scale_projects_panel.Name = "scale_projects_panel";
            this.scale_projects_panel.Size = new System.Drawing.Size(142, 390);
            this.scale_projects_panel.TabIndex = 0;
            // 
            // toolStripButtonExpert
            // 
            this.toolStripButtonExpert.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonExpert.Name = "toolStripButtonExpert";
            this.toolStripButtonExpert.Size = new System.Drawing.Size(64, 22);
            this.toolStripButtonExpert.Text = "Эксперты";
            this.toolStripButtonExpert.Click += new System.EventHandler(this.toolStripButtonExpert_Click);
            // 
            // Scale
            // 
            this.Scale.Image = global::MED.Properties.Resources.Scale_icon1;
            this.Scale.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Scale.Name = "Scale";
            this.Scale.Size = new System.Drawing.Size(63, 22);
            this.Scale.Text = "Шкала";
            this.Scale.Click += new System.EventHandler(this.Scale_Click);
            // 
            // Grades
            // 
            this.Grades.Enabled = false;
            this.Grades.Image = global::MED.Properties.Resources.criteria;
            this.Grades.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Grades.Name = "Grades";
            this.Grades.Size = new System.Drawing.Size(69, 22);
            this.Grades.Text = "Оценки";
            this.Grades.Click += new System.EventHandler(this.Grades_Click);
            // 
            // toolStripButtonResults
            // 
            this.toolStripButtonResults.Enabled = false;
            this.toolStripButtonResults.Image = global::MED.Properties.Resources.calculator2;
            this.toolStripButtonResults.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonResults.Name = "toolStripButtonResults";
            this.toolStripButtonResults.Size = new System.Drawing.Size(89, 22);
            this.toolStripButtonResults.Text = "Результаты";
            this.toolStripButtonResults.Click += new System.EventHandler(this.toolStripButtonResults_Click);
            // 
            // новыйПроектToolStripMenuItem
            // 
            this.новыйПроектToolStripMenuItem.Image = global::MED.Properties.Resources.New_document;
            this.новыйПроектToolStripMenuItem.Name = "новыйПроектToolStripMenuItem";
            this.новыйПроектToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.новыйПроектToolStripMenuItem.Text = "Новый файл";
            this.новыйПроектToolStripMenuItem.Click += new System.EventHandler(this.новыйПроектToolStripMenuItem_Click);
            // 
            // открытьПроектToolStripMenuItem
            // 
            this.открытьПроектToolStripMenuItem.Image = global::MED.Properties.Resources.open_file_icon;
            this.открытьПроектToolStripMenuItem.Name = "открытьПроектToolStripMenuItem";
            this.открытьПроектToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.открытьПроектToolStripMenuItem.Text = "Открыть файл";
            this.открытьПроектToolStripMenuItem.Click += new System.EventHandler(this.открытьПроектToolStripMenuItem_Click);
            // 
            // saveFileToolStripMenuItem
            // 
            this.saveFileToolStripMenuItem.Image = global::MED.Properties.Resources.saveIcon;
            this.saveFileToolStripMenuItem.Name = "saveFileToolStripMenuItem";
            this.saveFileToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.saveFileToolStripMenuItem.Text = "Сохранить";
            this.saveFileToolStripMenuItem.Click += new System.EventHandler(this.saveFileToolStripMenuItem_Click);
            // 
            // сохранитьToolStripMenuItem
            // 
            this.сохранитьToolStripMenuItem.Image = global::MED.Properties.Resources.save;
            this.сохранитьToolStripMenuItem.Name = "сохранитьToolStripMenuItem";
            this.сохранитьToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.сохранитьToolStripMenuItem.Text = "Сохранить как ...";
            this.сохранитьToolStripMenuItem.Click += new System.EventHandler(this.сохранитьToolStripMenuItem_Click);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Image = global::MED.Properties.Resources.close;
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.Image = global::MED.Properties.Resources.Help;
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.справкаToolStripMenuItem.Text = "Справка";
            this.справкаToolStripMenuItem.Click += new System.EventHandler(this.справкаToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(919, 532);
            this.Controls.Add(this.scale_control);
            this.Controls.Add(this.resultContainer);
            this.Controls.Add(this.СhiefContainer);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "MainForm";
            this.Text = "Метод экспертных оценок";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.СhiefContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.СhiefContainer)).EndInit();
            this.СhiefContainer.ResumeLayout(false);
            this.GradesControl.ResumeLayout(false);
            this.GradesCriteriaPage.ResumeLayout(false);
            this.GradesMeasurePage.ResumeLayout(false);
            this.resultContainer.Panel1.ResumeLayout(false);
            this.resultContainer.Panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resultContainer)).EndInit();
            this.resultContainer.ResumeLayout(false);
            this.scale_control.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem открытьПроектToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton toolStripButtonExpert;
        private System.Windows.Forms.ToolStripButton toolStripButtonCriteria;
        private System.Windows.Forms.ToolStripButton toolStripButtonMeasure;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonResults;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel info;
        private System.Windows.Forms.SplitContainer СhiefContainer;

        private System.Windows.Forms.TabControl GradesControl;
        private System.Windows.Forms.TabPage GradesMeasurePage;
        private System.Windows.Forms.TabPage GradesCriteriaPage;
        private System.Windows.Forms.ToolStripButton Grades;
        private System.Windows.Forms.Panel gradesCriteriaPanel;
        private System.Windows.Forms.Panel gradesMeasurePanel;
        private System.Windows.Forms.ToolStripButton Scale;
        private System.Windows.Forms.SplitContainer resultContainer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button getResult;
        private System.Windows.Forms.Panel resultExpertBox;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.TabControl scale_control;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel scale_criteria_panel;
        private System.Windows.Forms.Panel scale_projects_panel;
        private System.Windows.Forms.ToolStripMenuItem saveFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem новыйПроектToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
    }
}

