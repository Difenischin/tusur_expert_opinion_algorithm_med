﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Algo
{

    class PairwiseValidation
    {
        static public TaggedUnion<bool, string> is_pairwise_valid(double[,] matrix, double[] scale)
        {
            var size = matrix.GetLength(0);
            var max_error = scale[1];
            for (var i = 0; i < size; i++)
            {
                for (var j = 0; j < size; j++)
                {
                    if (i == j) continue;
                    if (matrix[i, j] > scale[0]) // if (matrix[i, j] > 0.0)
                    {
                        for (var k = 0; k < size; k++)
                        {
                            if (matrix[j, k] > scale[0]) // if (matrix[j, k] > 0)
                            {
                                if (!(matrix[i, k] > 0 &&
                                      (cond(matrix[i, j], matrix[j, k], matrix[i, k], max_error)
                                      || ((matrix[i, j] == matrix[j, k]) && (matrix[j, k] == matrix[i, k])))))
                                {
                                    return new TaggedUnion<bool, string>(String.Format("({0} {1} {2})", i + 1, j + 1, k + 1));
                                }
                            }
                        }
                    }
                }
            }

            return new TaggedUnion<bool, string>(true);
        }

        static private bool cond(double aIJ, double aJK, double aIK, double max_error)
        {
            return aIJ + aJK - aIK <= max_error;
        }
    }

    class Weights
    {
        /// <summary>
        /// Рассчет весов критериев по матрице попарных сравнений.
        /// </summary>
        public static double[] weights(double[,] pairwiseM, double[] scale)
        {
            return MED.MatrixRoutines.normalize_itself(MED.MatrixRoutines.row_sums(add_to_all(pairwiseM, scale)));
        }

        private static double[,] add_to_all(double[,] matrix, double[] scale)
        {
            var n_rows = matrix.GetLength(0);
            var n_cols = matrix.GetLength(1);
            var max_scale = scale[scale.GetLength(0) - 1];
            var min_scale = scale[0];

            double[,] result = new double[n_rows, n_cols];

            for (var i = 0; i < n_rows; i++)
                for (var j = 0; j < n_cols; j++)
                {
                    if (matrix[i, j] == min_scale) result[i, j] = 0;
                    else result[i, j] = matrix[i, j] + max_scale;
                }

            return result;
        }
    }

    class PairLooseMatrix
    {
        public static double[][][,] pairwise(double[][,] score_matrix)
        {
            var n_criterias = score_matrix[0].GetLength(0);
            var n_projects = score_matrix[0].GetLength(1);
            var n_experts = score_matrix.GetLength(0);

            double[][][,] result = new double[n_experts][][,];

            for (int i = 0; i < n_experts; i++)
            {
                result[i] = new double[n_criterias][,];

                for (int j = 0; j < n_criterias; j++)
                {
                    result[i][j] = new double[n_projects, n_projects];
                }
            }


            for (var expert = 0; expert < n_experts; expert++)
            {
                for (var criteria = 0; criteria < n_criterias; criteria++)
                {
                    for (var project_1 = 0; project_1 < n_projects; project_1++)
                    {
                        for (var project_2 = project_1 + 1; project_2 < n_projects; project_2++)
                        {
                            var comparison = 0.0;
                            if (score_matrix[expert][criteria, project_1] < score_matrix[expert][criteria, project_2])
                            { comparison = 1.0; }
                            if (score_matrix[expert][criteria, project_1] == score_matrix[expert][criteria, project_2])
                            { comparison = 0.0; }
                            if (score_matrix[expert][criteria, project_1] > score_matrix[expert][criteria, project_2])
                            { comparison = -1.0; }

                            result[expert][criteria][project_1, project_2] = comparison;
                            result[expert][criteria][project_2, project_1] = -comparison;
                        }


                    }
                }
            }

            return result;
        }
    }
    class Folding
    {
        static public double[] projects_score(double[][,] pairwiseM, double[] weightsM)
        {
            var n_projects = pairwiseM[0].GetLength(0);
            var n_criterias = pairwiseM.GetLength(0);
            var loseM = loose_matrix(pairwiseM, weightsM);
            var folded = fold_scores(loseM);
            return folded;
        }

        #region Приватные рутины
        static private double[] fold_scores(double[,] loose_mat)
        {
            var n_projects = loose_mat.GetLength(0);
            var cur_rating = 1.0;
            bool[] dropped = new bool[n_projects];
            double[] scores = new double[n_projects];
            double[] result = new double[n_projects];
            var m_copy = loose_mat;


            for (var times = 0; times < n_projects; times++)
            {
                /*
                 * 1. Посчитать сумму по каждой строчке в отдельности
                 * 2. Найти позицию минимального значения.
                 * 3. Проект на этой позиции - очередной лучший. Далее он исключается из рассмотрения.
                 */
                var sums = rows_sum(m_copy, dropped);
                var ind = min_case(sums, dropped);
                dropped[ind] = true;
                result[ind] = cur_rating;
                cur_rating += 1;
            }

            return result;
        }

        static private double[,] loose_matrix(double[][,] pairwiseM, double[] weightsM)
        {
            var n_projects = pairwiseM[0].GetLength(0);
            var n_citerias = weightsM.GetLength(0);

            double[,] folded = new double[n_projects, n_projects];

            for (int i = 0; i < n_projects; i++)
                for (var j = 0; j < n_projects; j++)
                {
                    if (i == j)
                    {
                        folded[i, j] = 0;
                        continue;
                    }
                    var lose_w = 0.0;

                    for (int k = 0; k < n_citerias; k++)
                    {
                        lose_w += weightsM[k] * Math.Abs(pairwiseM[k][i, j] - 1);
                    }

                    folded[i, j] = lose_w;
                }

            return folded;
        }

        /* Рассчет суммы по сторокам, не указанным в масииве dropped*/
        static private double[] rows_sum(double[,] mat, bool[] dropped)
        {
            var size = mat.GetLength(0);
            double[] result = new double[size];

            for (var i = 0; i < size; i++)
            {
                if (dropped[i])
                {
                    result[i] = double.MaxValue;
                    continue;
                }

                var sum = 0.0;

                for (var j = 0; j < size; j++)
                {
                    if (!dropped[j])
                    {
                        sum += mat[i, j];
                    }
                }

                result[i] = sum;
            }

            return result;
        }

        /*Грязный-грязный хак: поиск идекса минимума среди неотброшенных элементов матрицы*/
        static private int min_case(double[] mat, bool[] dropped)
        {
            var min = mat[0];
            var size = mat.GetLength(0);
            var result = 0;

            for (var i = 1; i < size; i++)
            {
                if (!dropped[i] && mat[i] < min)
                {
                    min = mat[i];
                    result = i;
                }
            }

            return result;
        }
        #endregion
    }

    class Results
    {
        static public double[] competee(double[,] experts_scores)
        {
            var n_experts = experts_scores.GetLength(0);
            var n_project = experts_scores.GetLength(1);
            double[] res = new double[n_experts];

            var x_s = mid(experts_scores);
            var lam = lambda(experts_scores);

            for (var i = 0; i < n_experts; i++)
            {
                var expert_comp = 0.0;

                for (var j = 0; j < n_project; j++)
                {
                    expert_comp += experts_scores[i, j] * x_s[j];
                }

                res[i] = expert_comp / lam;
            }

            return res;
        }
        static public void finalResults(double[,] priv_proj_rangs)
        {
            var md = mid(priv_proj_rangs);
            var sd = std_devs(priv_proj_rangs, md);
            var compet = competee(priv_proj_rangs);
            return;
        }
        static private double[] std_devs(double[,] mat, double[] expectation)
        {
            var n_rows = mat.GetLength(0);
            var n_cols = mat.GetLength(1);
            double[] res = new double[n_rows];

            for (var i = 0; i < n_rows; i++)
            {
                var sd = 0.0;

                for (var j = 0; j < n_cols; j++)
                {
                    sd += Math.Pow((mat[i, j] - expectation[j]), 2);
                }
                var sss = sd / n_cols;
                res[i] = Math.Sqrt(sss);
            }

            var sm = res.Sum();

            for (int i = 0; i < n_rows; i++)
            {
                res[i] = 1.0 - (res[i] / sm);
            }

            return res;
        }
        static public double[] mid(double[,] mat)
        {
            var n_rows = mat.GetLength(0);
            var n_cols = mat.GetLength(1);
            double[] res = new double[n_cols];

            for (var i = 0; i < n_cols; i++)
            {
                var sum = 0.0;

                for (var j = 0; j < n_rows; j++)
                {
                    sum += mat[j, i];
                }

                res[i] = sum / n_rows;
            }

            return res;
        }
        static private double lambda(double[,] experts_scores)
        {
            var n_experts = experts_scores.GetLength(0);
            var n_projects = experts_scores.GetLength(1);
            var mid_M = mid(experts_scores);
            var lam = 0.0;

            for (var i = 0; i < n_projects; i++)
            {
                for (var j = 0; j < n_experts; j++)
                {
                    lam += experts_scores[j, i] * mid_M[i];
                }
            }

            return lam;
        }
        static private double[] cols_sum(double[,] mat)
        {
            var n_rows = mat.GetLength(0);
            var n_cols = mat.GetLength(1);
            var result = new double[n_cols];

            for (var i = 0; i < n_cols; i++)
            {
                var sum = 0.0;

                for (var j = 0; j < n_rows; j++)
                {
                    sum += mat[j, i];
                }

                result[i] = sum;
            }

            return result;
        }

        static public double concardation(double[,] mat)
        {
            var n_experts = mat.GetLength(0);
            var n_projects = mat.GetLength(1);
            var sums = cols_sum(mat);
            var MO = (1.0 / n_projects) * sums.Sum();
            var dispersion_S = 0.0;

            for (var i = 0; i < n_projects; i++)
            {
                dispersion_S += Math.Pow((sums[i] - MO), 2);
            }

            var mul = 12.0 / (Math.Pow(n_experts, 2) * (Math.Pow(n_projects, 3) - n_projects));
            var concardation = mul * dispersion_S;

            return concardation;
        }
    }

    class Tests
    {
        public static void test()
        {
            testValidation();
            testWeights();
            testPairwise();
        }

        private static void testWeights()
        {
            double[] scale1 = { 0.0, 0.5, 1.0 };
            double[,] a = { { 0  , -0.5, -1  , -1   }, 
                            { 0.5, 0   , -0.5, -0.5 },
                            { 1  , 0.5 , 0   , -0.5 },
                            { 1  , 0.5 , 0.5 , 0    }};
            var b = Weights.weights(a, scale1);
            return;
        }

        private static void testPairwise()
        {
            double[] scale1 = { 0.0, 0.5, 1.0 };
            double[] scale2 = { 0.0, 3.0, 6.0, 9.0 };

            double[,] expert_1 = 
            {{10, 5, 4, 3, 5},
             {8 , 3, 2, 8, 5},
             {5 , 8, 6, 4, 5},
             {3 , 7, 2, 7, 5}};

            double[,] expert_2 =
            {{9, 4, 5, 3, 4},
             {9, 5, 4, 7, 6},
             {5, 8, 6, 6, 4},
             {2, 8, 3, 6, 5}};

            double[,] expert_3 =
            {{6, 10, 4, 6, 5},
             {7, 2 , 1, 5, 3},
             {8, 4 , 3, 2, 3},
             {5, 5 , 5, 6, 3}};

            double[,] exp1_pair = { { 0  , -0.5, -1  , -1   }, 
                                    { 0.5, 0   , -0.5, -0.5 },
                                    { 1  , 0.5 , 0   , -0.5 },
                                    { 1  , 0.5 , 0.5 , 0    }};

            double[,] exp2_pair = { { 0  , -1  , -1  , -1 }, 
                                    { 1  , 0   , -0.5, -1 },
                                    { 1  , 0.5 , 0   , -1 },
                                    { 1  , 1   , 1   , 0  }};

            double[,] exp3_pair = { { 0   ,  -0.5, -0.5, -0.5 }, 
                                    { 0.5 , 0    , 0   , 0.5  },
                                    { 0.5 , 0    , 0   , 1    },
                                    { -0.5, -0.5 , -1  , 0    }};

            double[][,] experts_scoreM = new double[3][,];
            experts_scoreM[0] = expert_1;
            experts_scoreM[1] = expert_2;
            experts_scoreM[2] = expert_3;

            double[][,] experts_pairM = new double[3][,];
            experts_pairM[0] = exp1_pair;
            experts_pairM[1] = exp2_pair;
            experts_pairM[2] = exp3_pair;

            var a = PairLooseMatrix.pairwise(experts_scoreM);

            var w_e1 = Weights.weights(exp1_pair, scale1);
            var w_e2 = Weights.weights(exp2_pair, scale1);
            var w_e3 = Weights.weights(exp3_pair, scale1);

            var scores_e1 = Folding.projects_score(a[0], w_e1);
            var scores_e2 = Folding.projects_score(a[1], w_e2);
            var scores_e3 = Folding.projects_score(a[2], w_e3);
            double[][] experts_res0 = new double[3][];
            experts_res0[0] = scores_e1;
            experts_res0[1] = scores_e2;
            experts_res0[2] = scores_e3;

            var experts_resultM = compress(experts_res0);

            var comp = Results.competee(experts_resultM);
            var concard = Results.concardation(experts_resultM);
            var final_result = Results.mid(experts_resultM);
            return;
        }

        private static void testValidation()
        {
            double[,] exp1_pair = { { 0  , -0.5, -1  , -1   }, 
                                    { 0.5, 0   , -0.5, -0.5 },
                                    { 1  , 0.5 , 0   , -0.5 },
                                    { 1  , 0.5 , 0.5 , 0    }};

            double[,] exp2_pair = { {  0,  3, 6,  6 }, 
                                    { -3,  0, 6, -3 },
                                    { -6, -6, 0, -3 },
                                    { -6,  3, 3,  0 }};

            double[,] exp3_pair = { {  0,  6,  6 }, 
                                    { -6,  0,  6 },
                                    { -6, -6,  0 }};

            double[] scale1 = { 0.0, 0.5, 1.0 };
            double[] scale2 = { 0.0, 3.0, 6.0, 9.0 };
            var valid1 = PairwiseValidation.is_pairwise_valid(exp1_pair, scale1);
            var valid2 = PairwiseValidation.is_pairwise_valid(exp2_pair, scale2);
            var valid3 = PairwiseValidation.is_pairwise_valid(exp3_pair, scale2);
            return;
        }

        public static double[,] compress(double[][] mat)
        {
            var n_rows = mat.GetLength(0);
            var n_cols = mat[0].GetLength(0);
            double[,] res = new double[n_rows, n_cols];

            for (var i = 0; i < n_rows; i++)
            {
                for (var j = 0; j < n_cols; j++)
                {
                    res[i, j] = mat[i][j];
                }
            }

            return res;
        }
        private bool equalM(double[,] a, double[,] b)
        {
            var a_rows = a.GetLength(0);
            var a_cols = a.GetLength(1);
            var b_rows = b.GetLength(0);
            var b_cols = b.GetLength(1);

            if (a_rows != b_rows || a_cols != b_cols) throw new Exception("Размеры матриц не совпадают!");

            for (int i = 0; i < a_rows; i++)
                for (int j = 0; j < a_cols; j++)
                {
                    if (a[i, j] != b[i, j]) return false;
                }

            return true;
        }
    }
    
}
