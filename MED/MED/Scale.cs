﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;

namespace MED
{
    class Scale
    {

    }

    public class ScaleEntity
    {
        public ScaleEntity() { }
        public ScaleEntity(double value_, string interpretation_)
        {
            this.value = value_;
            this.interpretation = interpretation_;
        }

        [DataMember]
        public double value;

        [DataMember]
        public String interpretation { get; set; }
    }
}
