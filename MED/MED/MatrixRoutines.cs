﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MED
{
    /// <summary>
    /// Класс, содержащий статическе методы для работы с
    /// матрицами
    /// </summary>
    class MatrixRoutines
    {
        /// <summary>
        /// Трансформирует матрицу в массив массивов.
        /// </summary>
        public static double[][] convertPlainToNested(double[,] original)
        {
            double[][] surrArray;
            int n_rows = original.GetLength(0);
            int n_cols = original.GetLength(1);
            surrArray = new double[n_rows][];

            for (int i = 0; i < n_rows; i++)
            {
                surrArray[i] = new double[n_cols];
                for (int j = 0; j < n_cols; j++)
                {
                    surrArray[i][j] = original[i, j];
                }
            }

            return surrArray;
        }

        /// <summary>
        /// Трансформирует массив массивов в матрицу.
        /// </summary>
        public static double[,] convertNestedToPlain(double[][] surrogate)
        {
            double[,] original;
            if (surrogate == null)
            {
                original = null;
            }
            else
            {
                int dimension0 = surrogate.Length;
                if (dimension0 == 0)
                {
                    original = new double[0, 0];
                }
                else
                {
                    int dimension1 = surrogate[0].Length;
                    for (int i = 1; i < dimension0; i++)
                    {
                        if (surrogate[i].Length != dimension1)
                        {
                            throw new InvalidOperationException("FUUUUUUUU");
                        }
                    }

                    original = new double[dimension0, dimension1];
                    for (int i = 0; i < dimension0; i++)
                    {
                        for (int j = 0; j < dimension1; j++)
                        {
                            original[i, j] = surrogate[i][j];
                        }
                    }
                }
            }
            return original;
        }

        /// <summary>
        /// Возвращает массив, содержащий суммы по строкам для матрицы.
        /// </summary>
        public static double[] row_sums(double[,] matrix)
        {
            var n_rows = matrix.GetLength(0);
            var n_cols = matrix.GetLength(1);
            double[] result = new double[n_rows];

            for (var i = 0; i < n_rows; i++)
            {
                var sum = 0.0;

                for (var j = 0; j < n_cols; j++)
                {
                    sum += matrix[i, j];
                }

                result[i] = sum;
            }

            return result;
        }

        /// <summary>
        /// Нормализует вектор по сумме своих элементов.
        /// </summary>
        public static double[] normalize_itself(double[] vector)
        {
            var len = vector.GetLength(0);
            double[] result = new double[len];

            var sum = vector.Sum();

            for (var i = 0; i < len; i++)
            {
                result[i] = vector[i] / sum;
            }

            return result;
        }

    }
}
