﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Algo
{
    class TaggedUnion<T1, T2>
    {
        private readonly bool isT1; // this is the tag; it could be implemented with an enum for more clarity
        private readonly object val;

        public T1 Val1
        {
            get
            {
                if (!isT1) throw new InvalidOperationException();
                return (T1)val;
            }
        }

        public T2 Val2
        {
            get
            {
                if (isT1) throw new InvalidOperationException();
                return (T2)val;
            }
        }

        public bool IsT1
        {
            get { return isT1; }
        }

        public TaggedUnion(T1 val)
        {
            this.val = val;
            isT1 = true;
        }

        public TaggedUnion(T2 val)
        {
            this.val = val;
            isT1 = false;
        }
    }
}
