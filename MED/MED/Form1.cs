﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MED
{

    public partial class MainForm : Form
    {
        #region declaration of variables
        DataGridView experts = new DataGridView();
        DataGridView criteria = new DataGridView();
        DataGridView measure = new DataGridView();
        DataGridView gradesCriteria = new DataGridView();
        DataGridView gradesMeasure = new DataGridView();
        DataGridView resultMeasure = new DataGridView();
        DataGridView criteria_scale = new DataGridView();
        DataGridView projects_scale = new DataGridView();
        DataGridView orderProjects = new DataGridView();
        State state = new State();
        State stateInFile = new State();
        private int globalExpertID = 1;
        private int globalCriteriaID = 1;
        private int globalMeasureID = 1;
        private int currentExpertIndex = 0;
        private string partToFile;
        #endregion

        #region initializeExpertsCriteriaMeasure
        void initializeExperts()
        {
            var columns = new List<Tuple<String, String>>();
            columns.Add(new Tuple<string, string>("lastName", "Фамилия"));
            columns.Add(new Tuple<string, string>("firstName", "Имя"));
            columns.Add(new Tuple<string, string>("patronymicName", "Отчество"));
            columns.Add(new Tuple<string, string>("idExpert","ID"));
            var expertsGrid = DataGridViewUtils.generateTable(columns);
            experts = expertsGrid;
            experts.Columns["idExpert"].Visible = false;
            experts.RowsAdded += GenExpertRowID;
            experts.RowsRemoved += experts_RowsRemoved;
        }
        void experts_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            ActivateButtons();
        }
        void GenExpertRowID(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (e.RowIndex > 0)
            {
                experts.Rows[e.RowIndex - 1].Cells["idExpert"].Value = globalExpertID.ToString();
                globalExpertID++;
                ActivateButtons();
            }
        }
        private void ValidateExpertRow(Object sender, DataGridViewCellCancelEventArgs data)
        {
            ActivateButtons();
        }


        void initializeCriteria()
        {
            var columns = new List<Tuple<String, String>>();
            columns.Add(new Tuple<string, string>("criteriaName", "Название критерия"));
            columns.Add(new Tuple<string, string>("idCriteria", "ID"));
            var criteriaGrid = DataGridViewUtils.generateTable(columns);
            criteria = criteriaGrid;
            criteria.Columns["idCriteria"].Visible = false;
            criteria.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
            criteria.RowValidating += criteria_RowValidating;
            criteria.RowsAdded += GenCriteriaRowID;
            criteria.RowsRemoved += criteria_RowsRemoved;
        }
        void criteria_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            ActivateButtons();
        }
        void GenCriteriaRowID(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (e.RowIndex > 0)
            {
                criteria.Rows[e.RowIndex - 1].Cells["idCriteria"].Value = globalCriteriaID.ToString();
                globalCriteriaID++;
            }
            else ActivateButtons();
        }
        void criteria_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
            ActivateButtons();
        }


        void initializeMeasure()
        {
            var columns = new List<Tuple<String, String>>();
            columns.Add(new Tuple<string, string>("measureName", "Название проекта"));
            columns.Add(new Tuple<string, string>("idMeasure", "ID"));
            var measureGrid = DataGridViewUtils.generateTable(columns);
            measure = measureGrid;
            measure.Columns["idMeasure"].Visible = false;
            measure.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
            measure.RowValidating += measure_RowValidating;
            measure.RowsAdded += genMeasureRowID;
            measure.RowsRemoved += measure_RowsRemoved;
        }
        void measure_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            ActivateButtons();
        }
        void genMeasureRowID(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (e.RowIndex > 0)
            {
                measure.Rows[e.RowIndex - 1].Cells["idMeasure"].Value = globalMeasureID.ToString();
                globalMeasureID++;
            }
            else ActivateButtons();
        }
        void measure_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
            ActivateButtons();
        }
        #endregion   

        #region initializeScale
        void initializeScale()
        {
            criteria_scale = new DataGridView();
            if (criteria_scale.Columns.Count != 0)
            {
                criteria_scale.Rows.Clear();
                criteria_scale.Columns.Clear();
            }
            criteria_scale.Columns.Add("value", "Значение");
            criteria_scale.Columns.Add("nameValue", "Интерпретация");
            criteria_scale.Columns[0].HeaderText = "Значение";

            projects_scale = new DataGridView();
            if (projects_scale.Columns.Count != 0)
            {
                projects_scale.Columns.Clear();
            }
            projects_scale.Columns.Clear();
            projects_scale.Columns.Add("value", "Значение");
            projects_scale.Columns.Add("nameValue", "Интерпретация");
            projects_scale.Columns[0].HeaderText = "Значение";


            Button setScale = new Button();
            setScale.Text = "Сохранить";
            setScale.Height = 30;
            setScale.UseVisualStyleBackColor = true;
            setScale.Click += setScale_Click;
            mainPanel.Controls.Clear();


            mainPanel.Controls.Add(setScale);
            
            mainPanel.Controls.Add(scale_control);

            setScale.Dock = DockStyle.Bottom;

            criteria_scale.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right);
            criteria_scale.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            criteria_scale.CellValidating += scale_CellValidating;
            criteria_scale.Dock = DockStyle.Fill;

            projects_scale.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right);
            projects_scale.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            projects_scale.CellValidating += scale_CellValidating;
            projects_scale.Dock = DockStyle.Fill;

            scale_criteria_panel.Controls.Clear();
            scale_projects_panel.Controls.Clear();
            scale_criteria_panel.Controls.Add(criteria_scale);
            scale_projects_panel.Controls.Add(projects_scale);
            //scale_control.Width = mainPanel.Width - scaleGroup.Width;
            scale_control.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right);
            scale_control.Dock = DockStyle.Fill;
            

        }
        void setScaleOfState(State state)
        {
            if (state.get_criteria_scale() != null)
            {
                for (int i = 0; i < state.get_criteria_scale().Count; i++)
                {
                    criteria_scale.Rows.Add(state.get_criteria_scale()[i].value.ToString(), state.get_criteria_scale()[i].interpretation);
                }
            }

            if (state.get_project_scale() != null)
            {
                for (int i = 0; i < state.get_project_scale().Count; i++)
                {
                    projects_scale.Rows.Add(state.get_project_scale()[i].value.ToString(), state.get_project_scale()[i].interpretation);
                }
            }
            
        }
        void scale_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            ((DataGridView)sender).Rows[e.RowIndex].ErrorText = "";
            double num;
            if (e.ColumnIndex == 0 && e.FormattedValue.ToString() != "")
            {
                string valueCells = e.FormattedValue.ToString(); //GUIUtils.replacementCommasOnPointInString(e.FormattedValue.ToString());
                if (!double.TryParse(valueCells, out num))
                {
                    e.Cancel = true;
                    try
                    {
                        criteria_scale.Rows[e.RowIndex].ErrorText = "Введите число!";
                    }
                    catch
                    {
                    }
                    info.Text = "Введите число!";
                }
                else
                {
                    info.Text = "";
                }
            }
        }
        void setScale_Click(object sender, EventArgs e)
        {
            if (scale_control.SelectedIndex == 0)
            {
                for (int i = 0; i < criteria_scale.RowCount - 1; i++)
                {
                    if ((criteria_scale.Rows[i].Cells[1].Value == null || criteria_scale.Rows[i].Cells[1].Value.ToString() == "") ||
                        (criteria_scale.Rows[i].Cells[0].Value == null || criteria_scale.Rows[i].Cells[0].Value.ToString() == ""))
                    {
                        MessageBox.Show("Необходимо присвоить значение каждой интерпретации", "Проверка вводимых данных", MessageBoxButtons.OK);
                        return;
                    }
                }
                state.set_criteria_scale(DataGridViewUtils.buildScale(criteria_scale));
                ActivateButtons();
            }
            else
            {
                for (int i = 0; i < projects_scale.RowCount - 1; i++)
                {
                    if ((projects_scale.Rows[i].Cells[0].Value == null || projects_scale.Rows[i].Cells[0].Value.ToString() == "") ||
                        (projects_scale.Rows[i].Cells[1].Value == null || projects_scale.Rows[i].Cells[1].Value.ToString() == ""))
                    {
                        MessageBox.Show("Необходимо присвоить значение каждой интерпретации", "Проверка вводимых данных", MessageBoxButtons.OK);
                        return;
                    }
                }
                state.set_project_scale(DataGridViewUtils.buildScale(projects_scale));
                ActivateButtons();
            }
        }
        #endregion

        #region initializeGrades
        private void initializeExpertBox()
        {
            Label name = new Label();
            name.Text = "Работает эксперт : ";
            name.Location = new Point(5, 5);
            СhiefContainer.Panel1.Controls.Clear();
            СhiefContainer.Panel1.Controls.Add(name);

            int startPositionRadioButton = 30;

            for (int i = 0; i < state.getExperts().Count; i++)
            {
                var exp = state.getExperts()[i];
                RadioButton expert = new RadioButton();
                expert.Name = i.ToString();
                expert.AutoSize = true;
                expert.Text = exp.fio.lastName + " " + exp.fio.name + " " + exp.fio.surName;
                expert.Location = new Point(10, startPositionRadioButton);
                СhiefContainer.Panel1.Controls.Add(expert);
                expert.CheckedChanged += expert_CheckedChanged;
                if (i == 0)
                {
                    expert.Checked = true;
                }

                int labelLocation = startPositionRadioButton + 30;
                Label a = new Label();
                a.AutoSize = true;
                a.Text = "Оценка критериев :";
                a.Location = new Point(20, labelLocation);
                СhiefContainer.Panel1.Controls.Add(a);

                Label GradesCriteriaL = new Label();
                GradesCriteriaL.AutoSize = true;
                GradesCriteriaL.Name = "GradesCriteria" + exp.fio.lastName + exp.fio.name + exp.fio.surName;
                GradesCriteriaL.Text = "Заполнена";
                GradesCriteriaL.Location = new Point(125, labelLocation);
                СhiefContainer.Panel1.Controls.Add(GradesCriteriaL);


                labelLocation += 20;
                Label b = new Label();
                b.AutoSize = true;
                b.Text = "Оценка проектов :";
                b.Location = new Point(20, labelLocation);
                СhiefContainer.Panel1.Controls.Add(b);


                Label GradesMeasureL = new Label();
                GradesMeasureL.AutoSize = true;
                GradesMeasureL.Name = "GradesMeasure" + exp.fio.lastName + exp.fio.name + exp.fio.surName;
                GradesMeasureL.Text = "Заполнена";
                GradesMeasureL.Location = new Point(125, labelLocation);
                СhiefContainer.Panel1.Controls.Add(GradesMeasureL);

                labelLocation += 20;
                Label conformityGradesCriteriaL = new Label();
                conformityGradesCriteriaL.AutoSize = true;
                conformityGradesCriteriaL.Name = "conformityGradesCriteria" + exp.fio.lastName + exp.fio.name + exp.fio.surName;
                conformityGradesCriteriaL.Text = "Согласованность неизвестна";
                conformityGradesCriteriaL.Location = new Point(20, labelLocation);
                СhiefContainer.Panel1.Controls.Add(conformityGradesCriteriaL);


                startPositionRadioButton += 90;
            }

        }
        private void expert_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton a = (RadioButton)sender;
            int indexExpert = int.Parse(a.Name);
            currentExpertIndex = indexExpert;
            var expertGrades = state.getExperts()[indexExpert].CriteriaGrades;
            DataGridViewUtils.SelIndChanged(gradesCriteria, expertGrades, DataGridViewUtils.resetGradesCriteria);

            expertGrades = state.getExperts()[indexExpert].ProjectGrades;

            DataGridViewUtils.SelIndChanged(gradesMeasure, expertGrades, DataGridViewUtils.resetGradesMeasure);
        }
        void initializeGradesCriteria()
        {
            var criterias = state.getCriterias();
            gradesCriteria = DataGridViewUtils.generateTable(GUIUtils.createCriteriaColumnsNames(state.getCriterias()));
            gradesCriteria.Name = "gradesCriteria";
            gradesCriteria.ShowCellToolTips = true;
            gradesCriteria.ColumnHeadersHeight = 50;
            //DataGridViewUtils.addRowsName(gradesCriteria, criteria.RowCount);
            DataGridViewUtils.addRowsName(gradesCriteria, criterias);
            DataGridViewUtils.dissallowEditingCells(gradesCriteria);
            gradesCriteria.AllowUserToDeleteRows = false;
            gradesCriteria.AllowUserToAddRows = false;
            gradesCriteria.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
            for (int i = 1; i < gradesCriteria.ColumnCount; i++)
            {
                gradesCriteria.Columns[i].ToolTipText = criterias[i - 1].name;
                gradesCriteria.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                gradesCriteria.Rows[i - 1].Cells[i].Value = state.get_criteria_scale()[0].value.ToString();
                gradesCriteria.Columns[i].DefaultCellStyle.Format = "N4";
            }

            if (!ScaleUtils.isScaleNullOrCountNull(state.get_criteria_scale()))
            {
                for (int i = 0; i < gradesCriteria.RowCount; i++)
                    for (int j = 1; j < gradesCriteria.ColumnCount; j++)
                        gradesCriteria.Rows[i].Cells[j].ToolTipText = ScaleUtils.tipTextScale(state.get_criteria_scale());
            }


            gradesCriteria.CellValidating += gradesCriteriaCellValidating;
            gradesCriteria.CellEnter += cancel_lower_part;
            gradesCriteria.CellDoubleClick += cancel_lower_part;

            //mainPanel.Controls.Add(gradesCriteria);
            gradesCriteriaPanel.Controls.Clear();
            gradesCriteriaPanel.Controls.Add(gradesCriteria);
        }

        /// <summary>
        /// Заблокировать табуляцию по нижней диагонали. 
        /// Если выбрана ячейка в нижней части матрицы,
        /// то осущевстляется "прыжок" в ближайшую
        /// доступную ячейку.
        /// </summary>
        void cancel_lower_part(object sender, DataGridViewCellEventArgs e)
        {
            var dtg = (DataGridView)sender;
            var cols_n = dtg.Columns.Count;
            var rows_n = dtg.Rows.Count;
            var sel_row = e.RowIndex;
            var sel_col = e.ColumnIndex;
            // если активирована ячейка ниже главной диагонали
            if ((sel_col - 1) <= sel_row)
            {
                dtg.Rows[sel_row].Cells[sel_col].Selected = false;
                var col_to_jump = sel_row + 2;
                if (col_to_jump > (cols_n - 1))
                {
                    dtg.Rows[0].Cells[2].Selected = true;
                }
                else
                    dtg.Rows[sel_row].Cells[col_to_jump].Selected = true;
            }
        }

        void gradesCriteriaCellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            gradesCriteria.Rows[e.RowIndex].ErrorText = "";
            double num;
            string valueCells = e.FormattedValue.ToString();// GUIUtils.replacementCommasOnPointInString(e.FormattedValue.ToString());
            Color standartColorCells = gradesCriteria.Rows[0].Cells[0].Style.BackColor;
            if (!(e.RowIndex >= e.ColumnIndex - 1) && valueCells != "")
            {
                if (!double.TryParse(valueCells, out num))
                {
                    e.Cancel = true;
                    gradesCriteria.Rows[e.RowIndex].ErrorText = "Необходимо ввести число!";
                    info.Text = "Необходимо ввести число!";
                }
                else
                {
                    if (ScaleUtils.is_value_in_criteria_scale(state.get_criteria_scale(), double.Parse(valueCells)))
                    {
                        info.Text = "";
                        var elem = (-1) * double.Parse(valueCells);
                        gradesCriteria.Rows[e.ColumnIndex - 1].Cells[e.RowIndex + 1].Value = elem.ToString();
                        gradesCriteria.Rows[e.ColumnIndex - 1].Cells[e.RowIndex + 1].Style.BackColor = standartColorCells;
                        gradesCriteria.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = standartColorCells;
                    }
                    else
                    {
                        e.Cancel = true;
                        gradesCriteria.Rows[e.RowIndex].ErrorText = "Значение находиться за пределам выбранной шкалы";
                        info.Text = "Значение находиться за пределам выбранной шкалы";
                    }
                }
            }

            if (!(e.RowIndex >= e.ColumnIndex - 1) && valueCells == "")
            {
                gradesCriteria.Rows[e.ColumnIndex - 1].Cells[e.RowIndex + 1].Value = "";
            }

        }
        private void checkValuesCellsGradesCriteriaOnScale()
        {
            Color standartColorCells = gradesCriteria.Rows[0].Cells[0].Style.BackColor;
            if (!ScaleUtils.isScaleNullOrCountNull(state.get_criteria_scale()))
            {
                for (int i = 0; i < gradesCriteria.RowCount; i++)
                    for (int j = 1; j < gradesCriteria.ColumnCount; j++)
                    {
                        if (gradesCriteria.Rows[i].Cells[j].Value != null)
                            if (gradesCriteria.Rows[i].Cells[j].Value.ToString() != "")
                            {
                                var valueCell = Math.Abs(double.Parse(gradesCriteria.Rows[i].Cells[j].Value.ToString()));
                                if (!ScaleUtils.isValueScaleExact(state.get_criteria_scale(), valueCell))
                                {
                                    gradesCriteria.Rows[i].Cells[j].Style.BackColor = Color.LightSkyBlue;
                                }
                                else
                                {
                                    gradesCriteria.Rows[i].Cells[j].Style.BackColor = standartColorCells;
                                }
                            }
                    }
            }
        }
        void saveCriteria_Click(object sender, EventArgs e)
        {
            double[,] matrixGrades = DataGridViewUtils.getMatrixGrades(gradesCriteria);
            state.getExperts()[currentExpertIndex].CriteriaGrades = matrixGrades;
            ActivateButtons();
            analysisMatricesExperts(СhiefContainer.Panel1);
            analysisСonformityMatrix(СhiefContainer.Panel1);
        }

        /// <summary>
        /// Проверка согласованноси матриц и отображение этого в GUI
        /// </summary>
        private void analysisСonformityMatrix(Panel output)
        {
            foreach (var exp in state.getExperts())
            {
                if (isMatrixNull(exp.CriteriaGrades) || exp.CriteriaGrades.GetLength(0) != state.getCriterias().Count)
                {
                    output.Controls["conformityGradesCriteria" + exp.fio.lastName + exp.fio.name + exp.fio.surName].Text = "Согласованность неизвестна";
                    output.Controls["conformityGradesCriteria" + exp.fio.lastName + exp.fio.name + exp.fio.surName].ForeColor = Color.Black;
                }
                else
                {
                    var matrix = exp.CriteriaGrades;

                    var valid_info = Algo.PairwiseValidation.is_pairwise_valid(matrix, ScaleUtils.scale_values(state.get_criteria_scale()));

                    if (valid_info.IsT1)
                    {
                        output.Controls["conformityGradesCriteria" + exp.fio.lastName + exp.fio.name + exp.fio.surName].Text =
                            "Матрица оценок критериев согласованна";
                        output.Controls["conformityGradesCriteria" + exp.fio.lastName + exp.fio.name + exp.fio.surName].ForeColor = Color.Black;
                    }
                    else
                    {
                        output.Controls["conformityGradesCriteria" + exp.fio.lastName + exp.fio.name + exp.fio.surName].Text =
                            "Противоречие в триаде " + valid_info.Val2;
                        output.Controls["conformityGradesCriteria" + exp.fio.lastName + exp.fio.name + exp.fio.surName].ForeColor = Color.Red;
                    }
                }
            }
        }
        private bool isMatrixCriteriaGradesСonformity(double[,] matrixCriteriaGrades, double[] scale)
        {
            var valid_info = Algo.PairwiseValidation.is_pairwise_valid(matrixCriteriaGrades, scale);
            return valid_info.IsT1;
        }

        void initializeGradesMeasure()
        {
            var measures = state.getMeasures();
            var criterias = state.getCriterias();
            gradesMeasure = DataGridViewUtils.generateTable(GUIUtils.createMeasuresColumnsNames(state.getMeasures()));
            DataGridViewUtils.addRowsName(gradesMeasure, criterias);

            gradesMeasure.Name = "gradesMeasure";
            gradesMeasure.ShowCellToolTips = true;
            gradesMeasure.Columns[0].ReadOnly = true;
            gradesMeasure.AllowUserToDeleteRows = false;
            gradesMeasure.AllowUserToAddRows = false;
            gradesMeasure.ColumnHeadersHeight = 50;
            gradesMeasure.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
            for (int i = 1; i < gradesMeasure.ColumnCount; i++)
            {
                gradesMeasure.Columns[i].ToolTipText = measures[i - 1].name;
                gradesMeasure.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                gradesMeasure.Columns[i].DefaultCellStyle.Format = "N4";
            }


            if (!ScaleUtils.isScaleNullOrCountNull(state.get_project_scale()))
            {
                for (int i = 0; i < gradesMeasure.RowCount; i++)
                    for (int j = 1; j < gradesMeasure.ColumnCount; j++)
                        gradesMeasure.Rows[i].Cells[j].ToolTipText = ScaleUtils.tipTextScale(state.get_project_scale());
            }

            gradesMeasure.CellValidating += gradesMeasureCellValidating;

            //mainPanel.Controls.Add(gradesMeasure);
            gradesMeasurePanel.Controls.Clear();
            gradesMeasurePanel.Controls.Add(gradesMeasure);
        }

        /// <summary>
        /// Валидация DataGrid с оценкам проектов.
        /// </summary>
        void gradesMeasureCellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            gradesMeasure.Rows[e.RowIndex].ErrorText = "";
            double num;
            string valueCells = e.FormattedValue.ToString();// GUIUtils.replacementCommasOnPointInString(e.FormattedValue.ToString());
            Color standartColorCells = gradesCriteria.Rows[0].Cells[0].Style.BackColor;
            if (e.ColumnIndex == 0 || valueCells == "") return;
            if (!double.TryParse(valueCells, out num))
            {
                e.Cancel = true;
                gradesMeasure.Rows[e.RowIndex].ErrorText = "Необходимо ввести число!";
                info.Text = "Необходимо ввести число!";
            }
            else
            {
                if (ScaleUtils.is_value_in_project_scale(state.get_project_scale(), double.Parse(valueCells)))
                {
                    info.Text = "";
                    gradesMeasure.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = standartColorCells;
                }
                else
                {
                    e.Cancel = true;
                    gradesMeasure.Rows[e.RowIndex].ErrorText = "Значение находиться за пределам выбранной шкалы";
                    info.Text = "Значение находиться за пределам выбранной шкалы";
                }
            }

        }
        private void checkValuesCellsGradesMeasureOnScale()
        {
            Color standartColorCells = gradesCriteria.Rows[0].Cells[0].Style.BackColor;
            if (!ScaleUtils.isScaleNullOrCountNull(state.get_project_scale()))
            {
                for (int i = 0; i < gradesMeasure.RowCount; i++)
                    for (int j = 1; j < gradesMeasure.ColumnCount; j++)
                    {
                        if (gradesMeasure.Rows[i].Cells[j].Value != null)
                            if (gradesMeasure.Rows[i].Cells[j].Value.ToString() != "")
                            {
                                var valueCell = Math.Abs(double.Parse(gradesMeasure.Rows[i].Cells[j].Value.ToString()));
                                if (!ScaleUtils.isValueScaleExact(state.get_project_scale(), valueCell))
                                {
                                    gradesMeasure.Rows[i].Cells[j].Style.BackColor = Color.LightSkyBlue;
                                }
                                else
                                {
                                    gradesMeasure.Rows[i].Cells[j].Style.BackColor = standartColorCells;
                                }
                            }
                    }
            }
        }
        void saveMeasure_Click(object sender, EventArgs e)
        {
            double[,] matrixGrades = DataGridViewUtils.getMatrixGrades(gradesMeasure);
            state.getExperts()[currentExpertIndex].ProjectGrades = matrixGrades;
            ActivateButtons();
            analysisMatricesExperts(СhiefContainer.Panel1);
        }

        private void GradesControl_Selected(object sender, TabControlEventArgs e)
        {
            int control = GradesControl.SelectedIndex;
            int gradesCriteria = 0;
            int gradesMeasure = 1;
            if (control == gradesCriteria)
                info.Text = "";
            if (control == gradesMeasure)
                info.Text = "Оцените";
        }

        private void analysisMatricesExperts(Panel output)
        {
            foreach (var exp in state.getExperts())
            {
                if (isMatrixNull(exp.CriteriaGrades) || exp.CriteriaGrades.GetLength(0) != state.getCriterias().Count)
                {
                    output.Controls["GradesCriteria" + exp.fio.lastName + exp.fio.name + exp.fio.surName].Text = "Не заполнена";
                    output.Controls["GradesCriteria" + exp.fio.lastName + exp.fio.name + exp.fio.surName].ForeColor = Color.Red;
                }
                else
                {
                    output.Controls["GradesCriteria" + exp.fio.lastName + exp.fio.name + exp.fio.surName].Text = "Заполнена";
                    output.Controls["GradesCriteria" + exp.fio.lastName + exp.fio.name + exp.fio.surName].ForeColor = Color.Green;
                }

                if (isMatrixNull(exp.ProjectGrades) || exp.ProjectGrades.GetLength(1) != state.getMeasures().Count)
                {
                    output.Controls["GradesMeasure" + exp.fio.lastName + exp.fio.name + exp.fio.surName].Text = "Не заполнена";
                    output.Controls["GradesMeasure" + exp.fio.lastName + exp.fio.name + exp.fio.surName].ForeColor = Color.Red;
                }
                else
                {
                    output.Controls["GradesMeasure" + exp.fio.lastName + exp.fio.name + exp.fio.surName].Text = "Заполнена";
                    output.Controls["GradesMeasure" + exp.fio.lastName + exp.fio.name + exp.fio.surName].ForeColor = Color.Green;
                }
            }
        }
        private bool isMatrixNull(double[,] a)
        {
            if (a == null) return true;
            foreach (var cell in a)
                if (cell == -987654321)
                    return true;
            return false;
        }
        #endregion

        #region initializeResult
        void initializeResultMeasure()
        {
            resultMeasure = DataGridViewUtils.generateTable(GUIUtils.createMeasuresColumnsNames(state.getMeasures()));
            resultMeasure.ShowCellToolTips = true;
            resultMeasure.ReadOnly = true;
            resultMeasure.Columns[0].HeaderText = "Эксперт";
            resultMeasure.AllowUserToDeleteRows = false;
            resultMeasure.AllowUserToAddRows = false;
            resultMeasure.ColumnHeadersHeight = 50;
            var Measures = state.getMeasures();
            for (int i = 1; i < resultMeasure.ColumnCount; i++)
                resultMeasure.Columns[i].ToolTipText = Measures[i - 1].name;

        }
        void initializeOrderProjects()
        {
            orderProjects = new DataGridView();
            orderProjects.Columns.Add("Проекты", "Проекты");
            orderProjects.Columns.Add("Коэффициент", "Коэффициент");
            orderProjects.ReadOnly = true;
            orderProjects.AllowUserToDeleteRows = false;
            orderProjects.AllowUserToAddRows = false;
            orderProjects.Columns[0].Width = 500;
            orderProjects.Height = 250;
            orderProjects.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
        }
        void initializeResultExpertBox()
        {
            int startPositionRadioButton = 0;

            for (int i = 0; i < state.getExperts().Count; i++)
            {
                var exp = state.getExperts()[i];
                CheckBox expert = new CheckBox();
                expert.Name = i.ToString();
                expert.AutoSize = true;
                expert.Text = exp.fio.lastName + " " + exp.fio.name + " " + exp.fio.surName;
                expert.Location = new Point(10, startPositionRadioButton);
                resultExpertBox.Controls.Add(expert);
                bool isMatrixGradesNull = isMatrixNull(exp.CriteriaGrades) || isMatrixNull(exp.ProjectGrades);
                if (!isMatrixGradesNull)
                {
                    bool isLengthMatrixGradesOk = exp.ProjectGrades.GetLength(1) == state.getMeasures().Count && exp.CriteriaGrades.GetLength(0) == state.getCriterias().Count;
                    if (isLengthMatrixGradesOk)
                    {
                        expert.Checked = isMatrixCriteriaGradesСonformity(exp.CriteriaGrades, ScaleUtils.scale_values(state.get_criteria_scale()));
                    }
                    else
                    {
                        expert.Enabled = false;
                        expert.Checked = false;
                    }
                }
                else
                {
                    expert.Enabled = false;
                    expert.Checked = false;
                }

                int labelLocation = startPositionRadioButton + 30;
                Label a = new Label();
                a.AutoSize = true;
                a.Text = "Оценка критериев :";
                a.Location = new Point(20, labelLocation);
                resultExpertBox.Controls.Add(a);

                Label GradesCriteriaL = new Label();
                GradesCriteriaL.AutoSize = true;
                GradesCriteriaL.Name = "GradesCriteria" + exp.fio.lastName + exp.fio.name + exp.fio.surName;
                GradesCriteriaL.Text = "Заполнена";
                GradesCriteriaL.Location = new Point(125, labelLocation);
                resultExpertBox.Controls.Add(GradesCriteriaL);


                labelLocation += 20;
                Label b = new Label();
                b.AutoSize = true;
                b.Text = "Оценка проектов :";
                b.Location = new Point(20, labelLocation);
                resultExpertBox.Controls.Add(b);


                Label GradesMeasureL = new Label();
                GradesMeasureL.AutoSize = true;
                GradesMeasureL.Name = "GradesMeasure" + exp.fio.lastName + exp.fio.name + exp.fio.surName;
                GradesMeasureL.Text = "Заполнена";
                GradesMeasureL.Location = new Point(125, labelLocation);
                resultExpertBox.Controls.Add(GradesMeasureL);

                labelLocation += 20;
                Label conformityGradesCriteriaL = new Label();
                conformityGradesCriteriaL.AutoSize = true;
                conformityGradesCriteriaL.Name = "conformityGradesCriteria" + exp.fio.lastName + exp.fio.name + exp.fio.surName;
                conformityGradesCriteriaL.Text = "Согласованность неизвестна";
                conformityGradesCriteriaL.Location = new Point(20, labelLocation);
                resultExpertBox.Controls.Add(conformityGradesCriteriaL);

                /*labelLocation += 20;
                Label с = new Label();
                с.AutoSize = true;
                с.Text = "Компетентность :";
                с.Location = new Point(20, labelLocation);
                resultExpertBox.Controls.Add(с);

                Label expertsCompetenceL = new Label();
                expertsCompetenceL.AutoSize = true;
                expertsCompetenceL.Name = "expertsCompetence" + exp.fio.lastName + exp.fio.name + exp.fio.surName;
                expertsCompetenceL.Text = "Не определена";
                expertsCompetenceL.Location = new Point(125, labelLocation);
                resultExpertBox.Controls.Add(expertsCompetenceL);*/

                startPositionRadioButton += 100;//120;
            }
        }
        private void addRowResultMeasure()
        {
            for (int i = 0; i < state.getExperts().Count; i++)
            {
                if (state.getExperts()[i].Active)
                    resultMeasure.Rows.Add(state.getExperts()[i].fio.lastName + " "
                        + state.getExperts()[i].fio.name + " "
                        + state.getExperts()[i].fio.surName);
            }
        }
        private void getResult_Click(object sender, EventArgs e)
        {
            setActiveExpert(state.getExperts());
            info.Text = "";
            resultMeasure.Rows.Clear();
            if (resultMeasure.Columns["comp"] != null) resultMeasure.Columns.Remove("comp");
            addRowResultMeasure();
            fillingAndCalculateResultMeasure();

        }
        private void setActiveExpert(List<Expert> experts)
        {
            for (int i = 0; i < experts.Count; i++)
            {
                var cb = (CheckBox)resultExpertBox.Controls[i.ToString()];
                experts[i].Active = cb.Checked;
            }
        }

        public void fillingAndCalculateResultMeasure()
        {           
            if (Algorithm.getActiveExpertsCount(state.getExperts()) == 0) return;

            var projImportance = getProjectImportanceActiveExperts(state);
            var competee = Algo.Results.competee(projImportance);
            var concard = Algo.Results.concardation(projImportance);
            var final_result = Algo.Results.mid(projImportance);


            for (var i = 0; i < resultMeasure.RowCount; i++)
                for (var j = 1; j < resultMeasure.ColumnCount; j++)
                    resultMeasure.Rows[i].Cells[j].Value = projImportance[i, j - 1].ToString();

            resultMeasure.Rows.Add("Результирующий вектор");

            for (var j = 1; j < resultMeasure.ColumnCount; j++)
            {
                resultMeasure.Rows[resultMeasure.RowCount - 1].Cells[j].Value =
                    final_result[j - 1].ToString("f3");
                resultMeasure.Rows[resultMeasure.RowCount - 1].Cells[j].Style.BackColor = Color.LightBlue;
            }

            resultMeasure.Columns.Add("comp", "Компетентность");
            for (var j = 0; j < resultMeasure.RowCount - 1; j++)
            {
                resultMeasure.Rows[j].Cells[resultMeasure.ColumnCount - 1].Value = competee[j].ToString("f3");
                resultMeasure.Rows[j].Cells[resultMeasure.ColumnCount - 1].Style.BackColor = Color.LightBlue;
            }

            info.Text = "Коэффициент конкордации = " + concard.ToString("f3");

            fillingOrderProjects(final_result);

        }
        void fillingOrderProjects(double[] final_result)
        {
            if (final_result.Count() == 0) return;

            orderProjects.Rows.Clear();

            var max = final_result[0];
            var min = final_result[0];

            List<int> indexsCompletedElements = new List<int>();

            int indexMinElement = 0;

            for (int i = 0; i < final_result.Count(); i++)
                if (min > final_result[i])
                {
                    min = final_result[i];
                    indexMinElement = i;
                }

            for (int j = 0; j < final_result.Count(); j++)
            {
                int currentIndexMaxElement = indexMinElement;
                max = min;
                for (int i = 0; i < final_result.Count(); i++)
                {
                    if (!isIndexInList(indexsCompletedElements, i))
                    {
                        if (max < final_result[i])
                        {
                            max = final_result[i];
                            currentIndexMaxElement = i;
                        }
                    }
                }
                indexsCompletedElements.Add(currentIndexMaxElement);
                orderProjects.Rows.Add(state.getMeasures()[currentIndexMaxElement].name, max.ToString("f3"));
            }
        }
        bool isIndexInList(List<int> list, int i)
        {
            for (int j = 0; j < list.Count(); j++)
                if (list[j] == i) return true;
            return false;
        }

        private double[,] getProjectImportanceActiveExperts(State state)
        {
            var experts_n = state.getActiveExperts().Count;
            var projects_n = state.getMeasures().Count;
            var res = new double[experts_n][];

            for (var i = 0; i < experts_n; i++)
                res[i] = new double[projects_n];

            var activeExperts = state.getActiveExperts();
            
            var exps_project_scores =  get_Experts_project_scores( activeExperts);


            var pairwise_from_projects = Algo.PairLooseMatrix.pairwise(exps_project_scores);

            for (var exp = 0; exp < experts_n; exp++ )
            {
                var scale = from sc in state.get_criteria_scale()
                            select sc.value;

                var s1 = scale.ToArray();

                var weights = Algo.Weights.weights(activeExperts[exp].CriteriaGrades, s1);

                var scores = Algo.Folding.projects_score(pairwise_from_projects[exp], weights);

                res[exp] = scores;
            }

            return Algo.Tests.compress(res);
        }
        private double[][,] get_Experts_project_scores(List<Expert> experts)
        {
            var exps_project_scores = from e in experts
                                      select e.ProjectGrades;

            return exps_project_scores.ToArray();
        }
        #endregion 

        void ActivateButtons()
        {
            //var a = ScaleUtils.isScaleNullOrCountNull(state.get_project_scale());
            if (experts.RowCount > 1 && criteria.RowCount > 1 && measure.RowCount > 1 && ScaleUtils.isScaleNullOrCountNull(state.get_criteria_scale()) == false && ScaleUtils.isScaleNullOrCountNull(state.get_project_scale()) == false)
            {
                var currentState = updatedState();
                state.merge(currentState);
                toolStrip.Items["Grades"].Enabled = true;
                toolStrip.Items["toolStripButtonResults"].Enabled = true;// isResultButtonActive();
            }
            else
            {
                toolStrip.Items["toolStripButtonResults"].Enabled = false;
                toolStrip.Items["Grades"].Enabled = false;
            }
        }
        bool isResultButtonActive()
        {
            if (state.getExperts().Count == 0) return false;
            foreach (var a in state.getExperts())
            {
                if ( isMatrixNull( a.CriteriaGrades) || isMatrixNull( a.ProjectGrades) )
                    return false;
            }
            return true;
        }

        bool ValidateExpertsCriteriaMeasures()
        {
            return (DataGridViewUtils.isTableColsValid(experts, new string[] { "lastName" }) && //DataGridViewUtils.isTableColsValid(experts, new string[] { "firstName", "lastName" }) &&
                 DataGridViewUtils.isTableColsValid(measure, new string[] { "measureName" }) &&
                 DataGridViewUtils.isTableColsValid(criteria, new string[] { "criteriaName" }));
        }
        private State updatedState()
        {
            var newState = new State();

            for (int i = 0; i < experts.RowCount - 1; i++)
            {
                var row = experts.Rows[i];
                FIO oif = new FIO();
                oif.lastName = (String)row.Cells[experts.Columns["lastName"].Index].Value;
                oif.name = (String)row.Cells[experts.Columns["firstName"].Index].Value;
                oif.surName = (String)row.Cells[experts.Columns["patronymicName"].Index].Value;
                var idStr = row.Cells[experts.Columns["idExpert"].Index].Value.ToString();
                var id = int.Parse(idStr);
                Expert a = new Expert(oif, id);
                newState.addExpert(a);
            }

            for (int i = 0; i < measure.RowCount - 1; i++)
            {
                var row = measure.Rows[i];
                var name = (String)row.Cells[measure.Columns["measureName"].Index].Value;
                var idStr = row.Cells[measure.Columns["idMeasure"].Index].Value.ToString();
                var id = int.Parse(idStr);
                Measure a = new Measure();
                a.id = id;
                a.name = name;

                newState.addMeasure(a);
            }

            for (int i = 0; i < criteria.RowCount - 1; i++)
            {
                var row = criteria.Rows[i];
                var name = (String)row.Cells[criteria.Columns["criteriaName"].Index].Value;
                var idStr = row.Cells[criteria.Columns["idCriteria"].Index].Value.ToString();
                var id = int.Parse(idStr);
                Criteria a = new Criteria();
                a.id = id;
                a.name = name;

                newState.addCriteria(a);
            }

            return newState;
        }
        
        public MainForm()
        {
            InitializeComponent();
            initializeExperts();
            initializeMeasure();
            initializeCriteria();
            mainPanel.Hide();
            СhiefContainer.Hide();
            resultContainer.Hide();
            scale_control.Hide();
        }

        private void toolStripButtonExpert_Click(object sender, EventArgs e)
        {
            if (ValidateExpertsCriteriaMeasures())
            {
                СhiefContainer.Hide();
                resultContainer.Hide();
                resultContainer.Dock = DockStyle.None;
                СhiefContainer.Dock = DockStyle.None;
                mainPanel.Controls.Clear();
                mainPanel.Dock = DockStyle.Fill;
                mainPanel.Show();
                mainPanel.Controls.Add(experts);
                scale_control.Hide();
                info.Text = "* Поле фамилия обязательное для заполнения";
            }
        }
        private void toolStripButtonCriteria_Click(object sender, EventArgs e)
        {
            if (ValidateExpertsCriteriaMeasures())
            {
                СhiefContainer.Hide();
                resultContainer.Hide();
                resultContainer.Dock = DockStyle.None;
                СhiefContainer.Dock = DockStyle.None;
                mainPanel.Dock = DockStyle.Fill;
                mainPanel.Controls.Clear();
                mainPanel.Show();
                mainPanel.Controls.Add(criteria);
                scale_control.Hide();
                info.Text = "";
            }
        }
        private void toolStripButtonMeasure_Click(object sender, EventArgs e)
        {
            if (ValidateExpertsCriteriaMeasures())
            {
                СhiefContainer.Hide();
                resultContainer.Hide();
                resultContainer.Dock = DockStyle.None;
                СhiefContainer.Dock = DockStyle.None;
                mainPanel.Dock = DockStyle.Fill;
                mainPanel.Controls.Clear();
                mainPanel.Show();
                mainPanel.Controls.Add(measure);
                scale_control.Hide();
                info.Text = "";
            }
        }
        private void Scale_Click(object sender, EventArgs e)
        {
            if (ValidateExpertsCriteriaMeasures())
            {
                СhiefContainer.Hide();
                resultContainer.Hide();
                resultContainer.Dock = DockStyle.None;
                СhiefContainer.Dock = DockStyle.None;
                mainPanel.Dock = DockStyle.Fill;
                mainPanel.Controls.Clear();
                mainPanel.Show();
                scale_control.Show();

                var currentState = updatedState();
                state.merge(currentState);

                initializeScale();
                setScaleOfState(state);
                info.Text = "*Первым элементом в шкале оценки критериев указывается центр шкалы";
            }
        }
        private void Grades_Click(object sender, EventArgs e)
        {
            mainPanel.Hide();
            resultContainer.Hide();
            resultContainer.Dock = DockStyle.None;
            mainPanel.Dock = DockStyle.None;
            СhiefContainer.Dock = DockStyle.Fill;
            СhiefContainer.Show();
            scale_control.Hide();
            var currentState = updatedState();
            state.merge(currentState);

            initializeGradesCriteria();
            initializeGradesMeasure();
            initializeExpertBox();

            checkValuesCellsGradesCriteriaOnScale();
            checkValuesCellsGradesMeasureOnScale();


            Button saveCriteria = new Button();
            saveCriteria.Text = "Сохранить";
            saveCriteria.Height = 30;
            saveCriteria.BackColor = Color.White;
            saveCriteria.UseVisualStyleBackColor = true;
            gradesCriteriaPanel.Controls.Add(saveCriteria);
            saveCriteria.Dock = DockStyle.Bottom;
            saveCriteria.Click += saveCriteria_Click;

            Button saveMeasure = new Button();
            saveMeasure.Text = "Сохранить";
            saveMeasure.Height = 30;
            saveMeasure.BackColor = Color.White;
            saveMeasure.UseVisualStyleBackColor = true;
            gradesMeasurePanel.Controls.Add(saveMeasure);
            saveMeasure.Dock = DockStyle.Bottom;
            saveMeasure.Click += saveMeasure_Click;

            analysisMatricesExperts(СhiefContainer.Panel1);
            analysisСonformityMatrix(СhiefContainer.Panel1);
            info.Text = "";
            //info.Text = "Согласованность рассчитывается по десятибалльной шкале (пользовательская шкала преобразуется в десятибалльную шкалу)";
        }
        /// <summary>
        /// Обработчик для рассчета результатов.
        /// </summary>
        private void toolStripButtonResults_Click(object sender, EventArgs e)
        {
            СhiefContainer.Hide();
            mainPanel.Hide();
            СhiefContainer.Dock = DockStyle.None;
            mainPanel.Dock = DockStyle.None;
            resultContainer.Dock = DockStyle.Fill;
            resultContainer.Show();
            scale_control.Hide();
            resultExpertBox.Controls.Clear();
            resultContainer.Panel2.Controls.Clear();

            var currentState = updatedState();
            state.merge(currentState);


            initializeResultExpertBox();
            initializeResultMeasure();
            initializeOrderProjects();

            resultContainer.Panel2.Controls.Add(resultMeasure);

            resultMeasure.Dock = DockStyle.Fill;


            resultContainer.Panel2.Controls.Add(orderProjects);
            orderProjects.Dock = DockStyle.Bottom;
           

            analysisMatricesExperts(resultExpertBox);
            analysisСonformityMatrix(resultExpertBox);
            info.Text = "";
        }


        #region MenuClick
        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {//сохранить как
            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                return;
            partToFile = saveFileDialog1.FileName;
            saveCurrentStateInFile(partToFile);           
        }
        private void saveFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (partToFile == null || partToFile == "")
            {
                сохранитьToolStripMenuItem_Click(null, null);
            }
            else
            {
                saveCurrentStateInFile(partToFile);
            }
        }
        private void открытьПроектToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                return;
            partToFile = openFileDialog1.FileName;
            readStateFromFile(partToFile);
        }   
        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {      
            this.Close();
        }
        private void новыйПроектToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var currentState = updatedState();
            state.merge(currentState);
            if (!state.equals(stateInFile))
            {
                var resultDialog = MessageBox.Show("Сохранить внесённые изменения в файл?", "Создание нового проекта", MessageBoxButtons.YesNoCancel);
                if (resultDialog == DialogResult.Yes)
                {
                    if (partToFile == null || partToFile == "")
                    {
                        if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                            return;
                        partToFile = saveFileDialog1.FileName;
                        saveCurrentStateInFile(partToFile);
                    }
                    else
                    {
                        saveCurrentStateInFile(partToFile);
                    }
                    createNewProject();
                }

                if (resultDialog == DialogResult.No)
                {
                    createNewProject();
                }
            }
        }
        private void createNewProject()
        {
            mainPanel.Controls.Clear();
            this.Text = "Метод экспертных оценок";
            globalExpertID = 1;
            globalCriteriaID = 1;
            globalMeasureID = 1;
            currentExpertIndex = 0;
            stateInFile = new State();
            partToFile = "";
            initializeExperts();
            initializeMeasure();
            initializeCriteria();
            initializeScale();
            state = new State();
            mainPanel.Hide();
            СhiefContainer.Hide();
            ActivateButtons();
            toolStripButtonExpert_Click(null, null);
        }
        private void справкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string pathToHelp = Application.StartupPath;
            string fname = "HelpMED.html";
            string help = pathToHelp + @"\" + fname;
            Help.ShowHelp(this, help, HelpNavigator.Find, "");
        }



        private void setCriExpMeasure()
        {
            globalExpertID = 1;
            globalCriteriaID = 1;
            globalMeasureID = 1;

            if (experts.Rows.Count > 1)
                experts.Rows.Clear();
            if (measure.Rows.Count > 1)
                measure.Rows.Clear();
            if (criteria.Rows.Count > 1)
                criteria.Rows.Clear();

            foreach (var a in state.getExperts())
                experts.Rows.Add(a.fio.lastName, a.fio.name, a.fio.surName, a.Id);
            foreach (var a in state.getMeasures())
                measure.Rows.Add(a.name, a.id);
            foreach (var a in state.getCriterias())
                criteria.Rows.Add(a.name, a.id);

            globalExpertID++;
            globalCriteriaID++;
            globalMeasureID++;

            ActivateButtons();
        }
        private void readStateFromFile(string part)
        {
            try
            {
                state = Saver.readStateFromFile(part);
                setCriExpMeasure();
                mainPanel.Controls.Clear();
                toolStripButtonExpert_Click(null, null);
                stateInFile = Saver.readStateFromFile(part);
                this.Text = "Метод экспертных оценок  - " + part;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Опаньки", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void saveCurrentStateInFile(string part)
        {
            try
            {
                var currentState = updatedState();
                state.merge(currentState);
                state.setIDs(globalExpertID, globalMeasureID, globalCriteriaID);
                Saver.saveStateToFile(part, state);
                stateInFile = Saver.readStateFromFile(part); 
                this.Text = "Метод экспертных оценок  - " + part;
                MessageBox.Show("Документ сохранён", "Сохранение документа", MessageBoxButtons.OK);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Опаньки", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            var currentState = updatedState();
            state.merge(currentState);
            if ( !state.equals( stateInFile))
            {
                var resultDialog = MessageBox.Show("Сохранить внесённые изменения в файл?", "Завершение работы программы", MessageBoxButtons.YesNoCancel);
                if (resultDialog == DialogResult.Yes)
                {
                    if (partToFile == "" || partToFile == null)
                    {
                        if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                            e.Cancel = true;
                        return;
                        partToFile = saveFileDialog1.FileName;
                        saveCurrentStateInFile(partToFile);
                    }
                    else
                    {
                        saveCurrentStateInFile(partToFile);
                    }
                }
                if (resultDialog == DialogResult.Cancel)
                {
                   e.Cancel = true;      
                }
                  
                /*if (MessageBox.Show("Все не сохранённые в файл изменения будут утеряны. Вы хотите продолжить?", "Завершение работы программы", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    e.Cancel = false;
                else e.Cancel = true;*/
            }
        }

       
    }
}

/*#NoUse
   void expertsCriteriaCombobox_GotFocus(object sender, EventArgs e)
        {
            double[,] matrixGrades = DataGridViewUtils.getMatrixGrades(gradesCriteria);
            state.getExperts()[((ComboBox)sender).SelectedIndex].CriteriaGrades = matrixGrades;
            ActivateButtons();
        }
        void expertsCriteriaCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            var expertIndex = ((ComboBox)sender).SelectedIndex;
            var expertGrades = state.getExperts()[expertIndex].CriteriaGrades;
            DataGridViewUtils.SelIndChanged(gradesCriteria, expertGrades, DataGridViewUtils.resetGradesCriteria);
        }
  private void toolStripButtonGradeCriteria_Click(object sender, EventArgs e)
        {
            if (ValidateExpertsCriteriaMeasures())
            {
                mainPanel.Show();

                var currentState = updatedState();
                state.merge(currentState);

                mainPanel.Controls.Clear();

                initializeGradesCriteria();

                var expertsComboBox = GUIUtils.createExpertsCB(state);
                expertsComboBox.SelectedIndexChanged += expertsCriteriaCB_SelectedIndexChanged;
                expertsComboBox.GotFocus += expertsCriteriaCombobox_GotFocus;


                expertsComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
               
                mainPanel.Controls.Add(expertsComboBox);
                expertsCriteriaCB_SelectedIndexChanged(expertsComboBox, null);
                expertsComboBox.Dock = DockStyle.Top;

                info.Text = "";

               /* Label conformity = new Label();
                conformity.Name = "conformityMatrix";
                conformity.Text = "Согласованность матрицы оценок";

                Button getConformity = new Button();
                getConformity.Text = "Рассчитать согласованность матрицы оценок";

                mainPanel.Controls.Add(getConformity);
                mainPanel.Controls.Add(conformity);

                getConformity.Dock = DockStyle.Bottom;
                conformity.Dock = DockStyle.Bottom;

                getConformity.Click += getConformity_Click;
            }
        }
  void getConformity_Click(object sender, EventArgs e)
        {
            double[,] matrixGrades = DataGridViewUtils.getMatrixGrades(gradesCriteria);
            var a = MatrixСalculator.conformityMatrix(matrixGrades);
            mainPanel.Controls["conformityMatrix"].Text = "Согласованность матрицы оценок : " + a.Item1 + " ( " + a.Item2 + " )";
        }
        private void toolStripButtonGradeMeasure_Click(object sender, EventArgs e)
        {
            if (ValidateExpertsCriteriaMeasures())
            {
                //var a = updatedState();
                state.merge(updatedState());


                mainPanel.Controls.Clear();

                initializeGradesMeasure();

                var expertsComboBox = GUIUtils.createExpertsCB(state);
                expertsComboBox.SelectedIndexChanged += expertsMeasuresCB_SelectedIndexChanged;
                expertsComboBox.GotFocus += expertsMeasureComboBox_GotFocus;

                expertsComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));

                mainPanel.Controls.Add(expertsComboBox);
                expertsMeasuresCB_SelectedIndexChanged(expertsComboBox, null);
                expertsComboBox.Dock = DockStyle.Top;
                info.Text = "Проранжируйте проекты (показаны в столбцах) по критериям (показаны в строках) , ранг = 1 соответствует наилучшему проекту";
            }
        }
  void expertsMeasureComboBox_GotFocus(object sender, EventArgs e)
        {
            double[,] matrixGrades = DataGridViewUtils.getMatrixGrades(gradesMeasure);
            state.getExperts()[((ComboBox)sender).SelectedIndex].ProjectGrades = matrixGrades;
            ActivateButtons();
        }
        void expertsMeasuresCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            var expertIndex = ((ComboBox)sender).SelectedIndex;
            var expertGrades = state.getExperts()[expertIndex].ProjectGrades;
            DataGridViewUtils.SelIndChanged(gradesMeasure, expertGrades, DataGridViewUtils.resetGradesMeasure);
        }   
   void saveLastGreadsExpert()
        {
            if (mainPanel.Controls.Count == 0) return;
            if (mainPanel.Controls[0].Name == "") return;
            if (mainPanel.Controls[0].Name == gradesCriteria.Name)
                expertsCriteriaCombobox_GotFocus(mainPanel.Controls[1], null);
            if (mainPanel.Controls[0].Name == gradesMeasure.Name)
                expertsMeasureComboBox_GotFocus(mainPanel.Controls[1], null);
        }
 #*/
/*
 
 void setConformity(double[,] matrixGrades)//HERE
        {
            double[,] surrArray;
            int dimension0 = matrixGrades.GetLength(0);
            int dimension1 = matrixGrades.GetLength(1);
            surrArray = new double[dimension0, dimension1];
            for (int i = 0; i < dimension0; i++)
            {
                for (int j = 0; j < dimension1; j++)
                {
                    if (i == j)
                    {
                        surrArray[i,j] = 1;
                    }
                    if (i < j)
                    {
                        surrArray[i, j] = matrixGrades[i, j];
                        surrArray[j, i] = 1 / ( matrixGrades[i, j]);
                    }
                }
            }

            double [] sumInRow = new double[dimension0];
            double sumAll = 0;
            for (int i = 0; i < dimension0; i++)
            {
                sumInRow[i] = surrArray[i, 0];
                for (int j = 1; j < dimension1; j++)
                {
                    sumInRow[i] *= surrArray[i, j];
                }
                double y = (double)(1.0 / dimension0);
                var fik = Math.Pow(sumInRow[i], y);
                sumInRow[i] = fik;
                sumAll += fik;
            }

            for (int i = 0; i < dimension0; i++)
            {
                sumInRow[i] = sumInRow[i] / sumAll;
            }

            double[] lamda = new double[dimension0];
            double lamdaSum = 0;

            for (int i = 0; i < dimension0; i++)
            {
                double sum = 0;
                for (int j = 0; j < dimension1; j++)
                {
                    sum += surrArray[j, i];
                }
                lamda[i] = sum * sumInRow[i];
                lamdaSum += lamda[i];
            }


            double EN = (lamdaSum - dimension0) / (dimension0 - 1);
            double k = (double)(EN) / 0.58;
            double OC = EN / k;
            PrioritiesSelector a = new PrioritiesSelector();
         //   GeneralMatrix newMatrix = new GeneralMatrix(surrArray);
          //  a.ComputePriorities(newMatrix);

            /*AHPModel model = new AHPModel(matrixGrades.GetLength(0), 0);
            model.AddCriteria( surrArray);
            model.CalculateModel();
            var a = model.CalculatedCriteria;
            var ab = a.Consistency;

        }*/